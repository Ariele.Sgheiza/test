<?php
namespace Concrete\Package\ajax\Theme\Themeajax;
defined('C5_EXECUTE') or die('Access Denied.');
use Concrete\Core\Page\Theme\Theme;
class PageTheme extends Theme
{
    public function getThemeName()
    {
        return t('ajax');
    }

    public function getThemeDescription()
    {
        return t('Theme based off the ajax Bootstrap 4 HTML theme.');
    }
}
?>
