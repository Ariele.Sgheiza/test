<?php
namespace Concrete\Package\lien\Theme\Themelien;
defined('C5_EXECUTE') or die('Access Denied.');
use Concrete\Core\Page\Theme\Theme;
class PageTheme extends Theme
{
    public function getThemeName()
    {
        return t('lien');
    }

    public function getThemeDescription()
    {
        return t('Theme based off the lien Bootstrap 4 HTML theme.');
    }
}
?>
