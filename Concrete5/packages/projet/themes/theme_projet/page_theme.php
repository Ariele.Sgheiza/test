<?php  
namespace Concrete\Package\projet\Theme\Themeprojet;
defined('C5_EXECUTE') or die('Access Denied.');
use Concrete\Core\Page\Theme\Theme;
class PageTheme extends Theme
{

    public function getThemeName()
    {
        return t('projet');
    }

    public function getThemeDescription()
    {
        return t('Theme based off the cherche Bootstrap 4 HTML theme.');
    }
}


