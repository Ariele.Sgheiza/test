<?php
use Concrete\Core\Database\Connection;
use Concrete\Core\Database\Driver\PDOStatement;

defined('C5_EXECUTE') or die('Access Denied.');
$view->inc('elements/header.php');
?>
<?php

$idProjet = $_POST['modifier'];
$db = mysqli_connect('localhost','cui','portfoliounige','portfolio');
$sqlProjet = "SELECT Projet.*,Theme.nom_theme,Type.nom_type FROM Projet,ProjetTheme,Theme,Type WHERE Projet.id_projet = '".$idProjet."' AND Projet.id_projet = ProjetTheme.id_projet AND ProjetTheme.id_theme = Theme.id_theme AND Projet.id_type = Type.id_type";
$infoProj = mysqli_query($db, $sqlProjet);
while($projet = mysqli_fetch_array($infoProj)){
	$_SESSION['titre'] = $projet['titre_projet'];
	$_SESSION['resume'] = $projet['resume'];
	$_SESSION['contexte'] = $projet['contexte'];
	$_SESSION['deroul'] = $projet['der_proj'];
	$_SESSION['concl'] = $projet['concl_proj'];
	$_SESSION['option'] = $projet['option_proj'];
	$_SESSION['mot_cle'] = $projet['mot_cle'];
	$_SESSION['nom_theme'] = $projet[14];
	$_SESSION['nom_type'] = $projet[15];
}

?>

<form class='input' method="post" enctype="multipart/form-data">
<div id="fomulaire">
<div id="creation">
	<h1>Modification du projet</h1>
	<hr class="separator">

<input class = 'putTitre' type="text" name="titre" placeholder="Titre de votre projet" id="insert" value="<?php echo $_SESSION['titre'] ?>">
</div>

<div id="histoire">
	<h1>Raconte nous l'histoire de ton projet</h1>
	<hr class="separator">
<textarea class='putDesc' type="text" name="desc" placeholder="Ecrire pas les liens pour le visiter!" id="insert" value="<?php echo $desc ?>"><?php echo $_SESSION['resume'] ?></textarea>
<textarea class='putContext' type="text" name="context" placeholder="Contexte" id="insert" value="<?php echo $context ?>"><?php echo $_SESSION['contexte'] ?></textarea>
<textarea class='putDer' type="text" name="der" placeholder="Deroulement" id="insert" value="<?php echo $deroul ?>"><?php echo $_SESSION['deroul'] ?></textarea>
<textarea class='putConcl' type="text" name="concl" placeholder="Conclusion" id="insert" value="<?php echo $concl ?>"><?php echo $_SESSION['concl'] ?></textarea>
</div>

<div id="infos">
	<h1>Plus d'informations sur ton projet...</h1>
	<hr class="separator">
<input class='putCle' type="text" name="cle" placeholder="Mots clefs qui pevuent décrire votre projet" id="insert" value="<?php echo $_SESSION['mot_cle'] ?>">

<div id="theme">
<p>Sélectionner thème du projet:</p>
<select class='selTheme'  name="theme">
<?php
$db1 = mysqli_connect('localhost', 'cui', 'portfoliounige', 'portfolio');
$sql1 = "SELECT id_theme, nom_theme FROM Theme";
$res = mysqli_query($db1, $sql1);
echo "<option value='".$_SESSION['nom_theme']."'>".$_SESSION['nom_theme']."</option>";
while($rows = $res->fetch_assoc())
{
        $nom_theme = $rows['nom_theme'];
	$id_theme = $rows['id_theme'];
	if($_SESSION['nom_theme']!=$nom_theme){
       		echo "<option value='$id_theme'>$nom_theme</option>";
	}
}
?>
</select>
</div>

<div id="type">
<p>Sélectionner type du projet:</p>
<select class='selType'  name="type">
<?php
$db2 = mysqli_connect('localhost', 'cui', 'portfoliounige', 'portfolio');
$sql2 = "SELECT id_type, nom_type FROM Type";
$res = mysqli_query($db2, $sql2);
echo "<option value='".$_SESSION['nom_type']."'>".$_SESSION['nom_type']."</option>";
while($rows = $res->fetch_assoc())
{
	$nom_type = $rows['nom_type'];
	$id_type = $rows['id_type'];
	if($_SESSION['nom_type']!=$nom_type){
        echo "<option value='$id_type'>$nom_type</option>";
	}
}
?>
</select>
</div>

<textarea class='putOpt' type="text" name="opt" placeholder="Options projet" id="insert" value="<?php echo $opt ?>"><?php echo $_SESSION['option'] ?></textarea>
</div>

<div id="fichier">
	<h1>Ajoute ce que tu veux!</h1>
	<hr class="separator">

<!-- <form action="upload.php" method="post" enctype="multipart/form-data"> -->
<!--    <input type="text" name="foldername"/> -->

<label for="file" class="label-file">Choisissez un dossier:
<input class='file' type="file" name="files[]" id="files" multiple directory="" webkitdirectory="" mozdirectory="">
</label>

<!--    <input type="submit" value="Upload File" name="submit"> -->
<!-- </form> -->

<label class="label-file">Choisissez une image:
  <input type="hidden" name="MAX_FILE_SIZE" value="1000000" />
  <input name="userfile" type="file" />
</label>

<?php

if(isset($_POST['submit'])){

	$titre = $_POST['titre'];
	$resume = $_POST['desc'];
	$context = $_POST['context'];
	$der = $_POST['der'];
	$concl = $_POST['concl'];
	$cle = $_POST['cle'];
	$id_theme = $_POST['theme'];
	$id_type = $_POST['type'];
	$lien = $_POST['lien'];
	$opt = $_POST['opt'];
	$db = mysqli_connect('localhost','cui','portfoliounige','portfolio');
	$db->query("UPDATE Projet SET titre_projet = '".$titre."' WHERE id_projet='".$_SESSION['id']."'");
	$db->query("UPDATE Projet SET resume = '".$resume."' WHERE id_projet='".$_SESSION['id']."'");
	$db->query("UPDATE Projet SET contexte = '".$context."' WHERE id_projet='".$_SESSION['id']."'");
	$db->query("UPDATE Projet SET der_proj = '".$der."' WHERE id_projet='".$_SESSION['id']."'");
	$db->query("UPDATE Projet SET concl_proj = '".$concl."' WHERE id_projet='".$_SESSION['id']."'");
	$db->query("UPDATE Projet SET option_proj = '".$opt."' WHERE id_projet='".$_SESSION['id']."'");
	$db->query("UPDATE Projet SET mot_cle = '".$cle."' WHERE id_projet='".$_SESSION['id']."'");
	$db->query("UPDATE ProjetTheme SET id_theme = '".$id_theme."' WHERE id_projet='".$_SESSION['id']."'");
	$db->query("UPDATE Projet SET id_type = '".$id_type."' WHERE id_projet='".$_SESSION['id']."'");



$db3 = mysqli_connect('localhost', 'cui', 'portfoliounige', 'portfolio');
$mail_prof = "SELECT Projet.titre_projet,mail_prof FROM Projet,Cours WHERE Projet.id_projet = '".$_SESSION['id']."' AND Projet.id_cours = Cours.id_cours";
$res3 = mysqli_query($db3, $mail_prof);
while($rows = $res3->fetch_assoc())
{
	 $mail_proff = $rows['mail_prof'];
        $titre_projet = $rows['titre_projet'];

}
$to = $mail_proff;
                $subject = 'Validation Projet';
                $message = 'Le projet appelé '.$titre_projet.' a été modifié, validez le sur PortfolioCUI: http://129.194.10.121/index.php/ ';
                $headers = 'From: noreply@portfoliounige.com'."\r\n" .
                        'Reply-To: noreply@portfoliounige.com' . "\r\n" .
                        'X-Mailer: PHP/'.phpversion();
                mail($to, $subject, $message, $headers);







$foldername = $_SESSION['id'];

if(basename($_FILES['userfile']['name'])!=""){

unlink("/var/www/html/concrete5/packages/cherche/themes/theme_cherche/uploadsImages/".$foldername.".png");
unlink("/var/www/html/concrete5/packages/portfolio/themes/theme_portfolio/uploadsImages/".$foldername.".png");
unlink("/var/www/html/concrete5/packages/etudiant/themes/theme_etudiant/uploadsImages/".$foldername.".png");
//unlink("/var/www/html/concrete5/packages/cherche/themes/theme_cherche/uploadsImages/".$foldername.".png");

$uploaddir = "/var/www/html/concrete5/packages/cherche/themes/theme_cherche/uploadsImages/";
$uploadfile = $uploaddir . basename($_FILES['userfile']['name']);

//echo '<pre>';
if (move_uploaded_file($_FILES['userfile']['tmp_name'], $uploadfile)) {
    echo "Successo.\n";
//    rename($uploadfile , "/var/www/html/concrete5/packages/cherche/themes/theme_cherche/uploadsImages/".$foldername.".png");
    copy($uploadfile, "/var/www/html/concrete5/packages/portfolio/themes/theme_portfolio/uploadsImages/".$foldername.".png");
    copy($uploadfile, "/var/www/html/concrete5/packages/etudiant/themes/theme_etudiant/uploadsImages/".$foldername.".png");
rename($uploadfile , "/var/www/html/concrete5/packages/cherche/themes/theme_cherche/uploadsImages/".$foldername.".png");
} else {
    echo "Failure.\n";
}
}


//rename($uploadfile , "/var/www/html/concrete5/application/uploadsImages/".$foldername.".png");

//echo 'Here is some more debugging info:';
//print_r($_FILES);
//print "</pre>";

if(isset($id_here)){
		$down=0;
                $foldername = $id_here;
                if(!is_dir($foldername)) mkdir($foldername);
                rename("/var/www/html/concrete5/".$foldername , "/var/www/html/concrete5/application/uploads/".$foldername);
                $target_dir = "/var/www/html/concrete5/application/uploads/".$foldername;

                foreach ($_FILES['files']['name'] as $i => $name)
                {
                        if($_FILES['files']['name'][$i] == 0)
                        {
                                move_uploaded_file($_FILES['files']['tmp_name'][$i], $target_dir."/".$name);
                                //echo "File uploaded";
                        }
			$down=1;
                }echo "Folder is successfully uploaded.";
        }else echo "Upload folder is not set.";
/*if($ok==1)
{
	$dir = "/var/www/html/concrete5/application/uploads/".$foldername;
	//$rootPath = realpath($dir);
	$zip_file = 'file.zip';
	$zip = new ZipArchive();
	$zip->open($zip_file, ZipArchive::CREATE | ZipArchive::OVERWRITE);
	$files = new RecursiveIteratorIterator( new RecursiveDirectoryIterator($dir), RecursiveIteratorIterator::LEAVES_ONLY);

	foreach ($files as $name => $file)
		{
    		// Skip directories (they would be added automatically)
    		if (!$file->isDir())
    			{
        		// Get real and relative path for current file
        		$filePath = $file->getRealPath();
        		$relativePath = substr($filePath, strlen($dir) + 1);
			echo $file;
        		// Add current file to archive
        		$zip->addFile($filePath, $relativePath);
    		}
	}
	$zip->close();

	header('Content-Description: File Transfer');
	header('Content-Type: application/zip');
	header('Content-Disposition: attachment; filename='.basename($zip_file));
	header('Content-Transfer-Encoding: binary');
	header('Expires: 0');
	header('Cache-Control: must-revalidate');
	header('Pragma: public');
	header('Content-Length: ' . filesize($zip_file));
	ob_clean();
	flush();
	readfile($zip_file);

}*/

}

echo "<input type='hidden' name='idProjet' value=".$id_here.">";
//echo "<input type='hidden' name='projetCree' value='chill'>";
echo "<input class='valider' type='submit' name='submit' value='Continuer'>";
echo "</div>";
echo "</form>";

if(isset($_POST['submit'])){
session_start();
$_POST['idProjet'] = $id_here;
$_SESSION['id'] = $id_here;
$_SESSION['projetCree'] = True;
header('Location: /index.php/projet');
exit();
}
?>


<?php
$view->inc('elements/footer.php');

