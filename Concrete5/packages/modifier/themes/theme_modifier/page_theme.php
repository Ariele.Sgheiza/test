<?php  
namespace Concrete\Package\modifier\Theme\Thememodifier;
defined('C5_EXECUTE') or die('Access Denied.');
use Concrete\Core\Page\Theme\Theme;
class PageTheme extends Theme
{

    public function getThemeName()
    {
        return t('modifier');
    }

    public function getThemeDescription()
    {
        return t('Theme based off the modifier Bootstrap 4 HTML theme.');
    }
}


