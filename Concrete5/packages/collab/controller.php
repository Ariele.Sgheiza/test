<?php  
namespace Concrete\Package\collab;
defined('C5_EXECUTE') or die('Access Denied.');
use \Concrete\Core\Package\Package;
class Controller extends Package
{
    protected $pkgHandle = 'collab';
    protected $appVersionRequired = '8.5.1';
    protected $pkgVersion = '0.6';

    public function getPackageDescription()
    {
        return t('Adds the collab theme to your website.');
    }

    public function getPackageName()
    {
        return t('collab');
    }

    public function install()
    {
         parent::install();
    	$this->installContentFile('install/theme.xml');
        // Our custom package installation code will go here.
    }

    public function upgrade()
    {
        parent::upgrade();
	$this->installContentFile('install/theme.xml');
	// Our custom package upgrade code will go here.
    }
}

