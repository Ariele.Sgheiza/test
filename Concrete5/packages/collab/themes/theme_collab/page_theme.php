<?php  
namespace Concrete\Package\collab\Theme\Themecollab;
defined('C5_EXECUTE') or die('Access Denied.');
use Concrete\Core\Page\Theme\Theme;
class PageTheme extends Theme
{

    public function getThemeName()
    {
        return t('collab');
    }

    public function getThemeDescription()
    {
        return t('Theme based off the collab Bootstrap 4 HTML theme.');
    }
}


