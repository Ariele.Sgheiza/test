<?php
use Concrete\Core\Database\Connection;
use Concrete\Core\Database\Driver\PDOStatement;

defined('C5_EXECUTE') or die('Access Denied.');
$view->inc('elements/header.php');
?>
<?php
session_start();
$id_projet = $_SESSION['id'];
?>

<form class='input' method="post" enctype="multipart/form-data">
<div id="participant">
<h1>Sélectionner les participants au projet:</h1>
<p style=' padding:0px; border-bottom: 3px solid #D80669; margin-bottom: 20px; margin-top:2px;margin-right:auto; margin-left:auto; width:440px;'></p>
<select class='selPart'  name="part">
<option value='aucun'>Aucun</option>
<?php
$db2 = mysqli_connect('localhost', 'cui', 'portfoliounige', 'portfolio');
$sql2 = "SELECT mail_etu FROM Etudiant";
$res = mysqli_query($db2, $sql2);

$db3 = mysqli_connect('localhost', 'cui', 'portfoliounige', 'portfolio');
$sql3 = "SELECT mail_etu FROM EtudiantProjet, Etudiant WHERE EtudiantProjet.id_projet = '".$id_projet."' AND EtudiantProjet.id_etu = Etudiant.id_etu";
$res3 = mysqli_query($db3, $sql3);
$listeEtu = array();
while($mailscollab = $res3->fetch_assoc()){
    $listeEtu[] = $mailscollab['mail_etu'];
}

while($rows = $res->fetch_assoc())
{       $mail_etu = $rows['mail_etu'];
	if(!in_array($mail_etu,$listeEtu)){
        echo "<option value='$mail_etu'>$mail_etu</option>";
	}

}
?>
</select>
<div class='butons'>
<input class='ajouterCollab' type='submit' name='ajouterCollab' value='Ajouter collaborateur'>
<input class='ajouterExit' type='submit' name='exit' value='Valider'>
</div>
</div>
</form>

<?php
session_start();
$id_projet = $_SESSION['id'];
if(isset($_POST['ajouterCollab'])){
if($_POST['part']=="aucun"){
echo "<p class='error'>Vous pouvez valider le projet en appuyant sur 'Valider'</p>";
}else{
$db = \Database::connection();
$id_part = $db->fetchColumn("SELECT id_etu FROM Etudiant WHERE mail_etu=?", [$_POST['part']]);
$db->query("INSERT INTO EtudiantProjet (id_etu, id_projet) VALUES (?,?)", [$id_part, $id_projet]);
//header('Location: /index.php/collab');
header('Refresh: 0; url=/index.php/collab');
exit();
}
}

if(isset($_POST['exit'])){
if($_POST['part']!="aucun"){
$db = \Database::connection();
$id_part = $db->fetchColumn("SELECT id_etu FROM Etudiant WHERE mail_etu=?", [$_POST['part']]);
$db->query("INSERT INTO EtudiantProjet (id_etu, id_projet) VALUES (?,?)", [$id_part, $id_projet]);
}

$db3 = mysqli_connect('localhost', 'cui', 'portfoliounige', 'portfolio');
$mail_prof = "SELECT Projet.titre_projet,mail_prof FROM Projet,Cours WHERE Projet.id_projet = '".$id_projet."' AND Projet.id_cours = Cours.id_cours";
$res3 = mysqli_query($db3, $mail_prof);
while($rows = $res3->fetch_assoc())
{
	$mail_proff = $rows['mail_prof'];
	$titre_projet = $rows['titre_projet'];
}
                $to = $mail_proff;
                $subject = 'Validation Projet';
                $message = 'Le projet appelé '.$titre_projet.' est à valider sur le PortfolioCUI: http://129.194.10.121/index.php/ ';
                $headers = 'From: noreply@portfoliounige.com'."\r\n" .
                        'Reply-To: noreply@portfoliounige.com' . "\r\n" .
                        'X-Mailer: PHP/'.phpversion();
                mail($to, $subject, $message, $headers);

header('Location: /index.php/projet');
exit();
}
?>

<?php
$view->inc('elements/footer.php');
