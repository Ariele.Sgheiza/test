<?php
namespace Concrete\Package\Login\Theme\ThemeLogin;
defined('C5_EXECUTE') or die('Access Denied.');
use Concrete\Core\Page\Theme\Theme;
class PageTheme extends Theme
{
    public function getThemeName()
    {
        return t('Login');
    }

    public function getThemeDescription()
    {
        return t('Theme based off the Login Bootstrap 4 HTML theme.');
    }
}
?>
