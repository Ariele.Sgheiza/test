<?php
namespace Concrete\Package\portfolio;
defined('C5_EXECUTE') or die('Access Denied.');
use \Concrete\Core\Package\Package;
//	use Concrete\Core\Page\Theme\Theme;
class Controller extends Package
{
    protected $pkgHandle = 'portfolio';
    protected $appVersionRequired = '8.5.4';
    protected $pkgVersion = '0.6';

    public function getPackageDescription()
    {
        return t('Adds the CUI portfolio theme to your website.');
    }

    public function getPackageName()
    {
        return t('portfolio');
    }

    public function install()
    {
        $pkg = parent::install();
	// $theme = Theme::install(‘theme_portfolio’, $pkg); 
	$this->installContentFile('install/theme.xml'); 
    }

    public function upgrade()
    {
	    parent::upgrade();
	    $this->installContentFile('install/theme.xml');
    }
}

