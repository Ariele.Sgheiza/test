<?php  
namespace Concrete\Package\portfolio\Theme\ThemePortfolio;
use Concrete\Core\Page\Theme\Theme;
defined('C5_EXECUTE') or die('Access Denied.');
class PageTheme extends Theme
{

    public function getThemeName()
    {
        return t('portfolio');
    }

    public function getThemeDescription()
    {
        return t('Theme based off the Dreamrs Bootstrap 4 HTML theme.');
    }
}

