<?php
defined('C5_EXECUTE') or die('Access Denied.');
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <?php View::element('header_required'); ?>
    <link rel="stylesheet" href="<?=$view->getThemePath()?>/index.css">

</head>

<body>
    <div class="<?=$c->getPageWrapperClass()?>">
    <header id = "header_main">
        <img class="logoUnige" src="<?=$view->getThemePath()?>/images/logo.png" alt="logoUnige">

<?php
if(!isset($_SESSION['uname'])){
	echo "<a href='/index.php/connexion'>";
	echo "<span class='connexion'>Se Connecter</span>";
	echo "</a>";
}else{
	echo "<form class='formCon'  method='post' action='/index.php/etudiant' name='idEtu' value=".$_SESSION['idEtuCon'].">";
	echo "<input type='hidden' class='connexion' name='idEtu' value='".$_SESSION['idEtuCon']."'>";
	echo "<input type='submit' class='connexion' value='".$_SESSION['nomEtuCon']."'>";
	echo "</form>";
}
?>
	<div class="vl">
            <h1 class=titreMain>Portfolio</h1>
       	</div>
<?php
/*if(!isset($_SESSION['uname'])){
        echo "<a href='/index.php/connexion'>";
        echo "<span class='connexion'>Se Connecter</span>";
        echo "</a>";
}else{
        echo "<form  method='post' action='/index.php/etudiant' name='idEtu' value=".$_SESSION['idEtuCon'].">";
        echo "<input type='hidden' class='connexion' name='idEtu' value='".$_SESSION['idEtuCon']."'>";
        echo "<input type='submit' class='connexion' value='".$_SESSION['nomEtuCon']."'>";
        echo "</form>";
}*/
?>

        <p id="descMain">Découvrez les projets de nos étudiants en lançant une recherche.</p>
	<?php
	$a = new Area('search');
	$a->display($c);
	?>
	<a class="flechelink" href="#discover"><img class="fleche" src="<?=$view->getThemePath()?>/images/fleche.png" alt="fleche"></a>
    </header>
</div>
</body>
</html>


