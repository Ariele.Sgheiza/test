<?php  
namespace Concrete\Package\ajouter\Theme\Themeajouter;
defined('C5_EXECUTE') or die('Access Denied.');
use Concrete\Core\Page\Theme\Theme;
class PageTheme extends Theme
{

    public function getThemeName()
    {
        return t('ajouter');
    }

    public function getThemeDescription()
    {
        return t('Theme based off the cherche Bootstrap 4 HTML theme.');
    }
}


