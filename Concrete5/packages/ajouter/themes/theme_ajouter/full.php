<?php
use Concrete\Core\Database\Connection;
use Concrete\Core\Database\Driver\PDOStatement;

defined('C5_EXECUTE') or die('Access Denied.');
$view->inc('elements/header.php');
?>

<form class='input' method="post" enctype="multipart/form-data">
<div id="fomulaire">
<div id="creation">
	<h1>Création d'un projet</h1>
	<hr class="separator">

<input class = 'putTitre' type="text" name="titre" placeholder="Titre de votre projet" id="insert" value="<?php echo $titre ?>">
</div>

<div id="histoire">
	<h1>Raconte nous l'histoire de ton projet</h1>
	<hr class="separator">
<textarea class='putDesc' type="text" name="desc" placeholder="Ecrire ici la description de votre projet, n'oubliez pas les liens pour le visiter!" id="insert" value="<?php echo $desc?>"></textarea>
<textarea class='putContext' type="text" name="context" placeholder="Contexte" id="insert" value="<?php echo $context?>"></textarea>
<textarea class='putDer' type="text" name="der" placeholder="Deroulement" id="insert" value="<?php echo $der?>"></textarea>
<textarea class='putConcl' type="text" name="concl" placeholder="Conclusion" id="insert" value="<?php echo $concl?>"></textarea>
</div>

<div id="infos">
	<h1>Plus d'informations sur ton projet...</h1>
	<hr class="separator">
<input class='putCle' type="text" name="cle" placeholder="Mots clefs qui pevuent décrire votre projet" id="insert" value="<?php echo $cle?>">

<div id="cours">
<p>Sélectionner cours:</p>
<select class='selCours' name="choisir">
<?php
$db = mysqli_connect('localhost', 'cui', 'portfoliounige', 'portfolio');
$sql = "SELECT nom_cours FROM Cours";
$res = mysqli_query($db, $sql);
while($rows = $res->fetch_assoc())
{
        $nom_cours = $rows['nom_cours'];
        echo "<option value='$nom_cours'>$nom_cours</option>";
}
?>
</select>
</div>

<div id="theme">
<p>Sélectionner thème du projet:</p>
<select class='selTheme'  name="theme">
<?php
$db1 = mysqli_connect('localhost', 'cui', 'portfoliounige', 'portfolio');
$sql1 = "SELECT nom_theme FROM Theme";
$res = mysqli_query($db1, $sql1);
while($rows = $res->fetch_assoc())
{
        $nom_theme = $rows['nom_theme'];
        echo "<option value='$nom_theme'>$nom_theme</option>";
}
?>
</select>
</div>

<div id="type">
<p>Sélectionner type du projet:</p>
<select class='selType'  name="type">
<?php
$db2 = mysqli_connect('localhost', 'cui', 'portfoliounige', 'portfolio');
$sql2 = "SELECT nom_type FROM Type";
$res = mysqli_query($db2, $sql2);
while($rows = $res->fetch_assoc())
{        $nom_type = $rows['nom_type'];
        echo "<option value='$nom_type'>$nom_type</option>";
}
?>
</select>
</div>

<textarea class='putOpt' type="text" name="opt" placeholder="Options projet" id="insert" value="<?php echo $opt?>"></textarea>
</div>

<div id="fichier">
	<h1>Ajoute ce que tu veux!</h1>
	<hr class="separator">

<!-- <form action="upload.php" method="post" enctype="multipart/form-data"> -->
<!--    <input type="text" name="foldername"/> -->

<label for="file" class="label-file">Choisissez un dossier:
<input class='file' type="file" name="files[]" id="files" multiple directory="" webkitdirectory="" mozdirectory="">
</label>

<!--    <input type="submit" value="Upload File" name="submit"> -->
<!-- </form> -->

<label class="label-file">Choisissez une image:
  <input type="hidden" name="MAX_FILE_SIZE" value="1000000" />
  <input name="userfile" type="file" />
</label>

<?php

if(isset($_POST['submit'])){
	//$filename = $_FILES["files"]["name"];


	$ok=0;
	$titre = $_POST['titre'];
	$desc = $_POST['desc'];
	$context = $_POST['context'];
	$der = $_POST['der'];
	$concl = $_POST['concl'];
	$cle = $_POST['cle'];
	$down = $_POST['download'];
	if($down == "oui") $ok = 1;
	$cours = $_POST['choisir'];
	$theme = $_POST['theme'];
	$type = $_POST['type'];
	$lien = $_POST['lien'];
	$opt = $_POST['opt'];
	$idEtu = $_SESSION['idEtuCon'];
	$mail_etu = $_POST['part'];
	$db = \Database::connection();
	$id_part = $db->fetchColumn("SELECT id_etu FROM Etudiant WHERE mail_etu=?", [$mail_etu]);
	$id_type = $db->fetchColumn("SELECT id_type FROM Type WHERE nom_type=?", [$type]);
	$id=$db->fetchColumn("SELECT id_cours FROM Cours WHERE nom_cours=?", [$cours]);
	if($id=='') $id='D200018';
	$db->query("INSERT INTO Projet (titre_projet, resume, contexte, der_proj,concl_proj, option_proj, mot_cle, id_cours, id_type) VALUES (?,?,?,?,?,?,?,?,?)", [$titre,$desc,$context,$der,$concl,$opt,$cle,$id,$id_type]);
	$id_theme = $db->fetchColumn("SELECT id_theme FROM Theme WHERE nom_theme=?", [$theme]);
	//$id_type = $db->fetchColumn("SELECT id_type FROM Type WHERE nom_type=?", [$type]);
	$id_here = $db->fetchColumn("SELECT id_projet FROM Projet WHERE titre_projet=?",[$titre]);
	$db->query("INSERT INTO ProjetTheme (id_projet, id_theme) VALUES (?,?)", [$id_here, $id_theme]);
	$db->query("INSERT INTO EtudiantProjet (id_etu, id_projet) VALUES (?,?)", [$idEtu, $id_here]);
	//$db->query("INSERT INTO EtudiantProjet (id_etu, id_projet) VALUES (?,?)", [$id_part, $id_here]);
	//$db->query("INSERT INTO Projet (id_type) VALUES")
	//$db->query("INSERT INTO Projet (titre_projet, resume, contexte, der_proj,concl_proj, mot_cle, telecharger, id_cours, id_type) VALUES (?,?,?,?,?,?,?,?,?)", [$titre,$desc,$context,$der,$concl,$cle,$ok,$id, $id_type]);


$foldername = $id_here;

$uploaddir = "/var/www/html/concrete5/packages/cherche/themes/theme_cherche/uploadsImages/";
$uploadfile = $uploaddir . basename($_FILES['userfile']['name']);

//echo '<pre>';
if (move_uploaded_file($_FILES['userfile']['tmp_name'], $uploadfile)) {
    echo "Success.\n";
//    rename($uploadfile , "/var/www/html/concrete5/packages/cherche/themes/theme_cherche/uploadsImages/".$foldername.".png");
    copy($uploadfile, "/var/www/html/concrete5/packages/portfolio/themes/theme_portfolio/uploadsImages/".$foldername.".png");
    copy($uploadfile, "/var/www/html/concrete5/packages/etudiant/themes/theme_etudiant/uploadsImages/".$foldername.".png");
rename($uploadfile , "/var/www/html/concrete5/packages/cherche/themes/theme_cherche/uploadsImages/".$foldername.".png");
} else {
    echo "Failure.\n";
}

//rename($uploadfile , "/var/www/html/concrete5/application/uploadsImages/".$foldername.".png");
//echo 'Here is some more debugging info:';
//print_r($_FILES);
//print "</pre>";


/*if(isset($id_here)){

$arr_file_types = ['image/png', 'image/gif', 'image/jpg', 'image/jpeg'];

if (!(in_array($_FILES['file']['type'], $arr_file_types))) {
    echo "false";
    return;
}

if (!file_exists('uploads')) {
    mkdir('uploads', 0777);
}

$filename = time().'_'.$_FILES['file']['name'];

move_uploaded_file($_FILES['file']['tmp_name'], 'uploads/'.$filename);

echo 'uploads/'.$filename;
die;


}*/





if(isset($id_here)){
		$down=0;
                $foldername = $id_here;
                if(!is_dir($foldername)) mkdir($foldername);
                rename("/var/www/html/concrete5/".$foldername , "/var/www/html/concrete5/application/uploads/".$foldername);
                $target_dir = "/var/www/html/concrete5/application/uploads/".$foldername;

                foreach ($_FILES['files']['name'] as $i => $name)
                {
                        if($_FILES['files']['name'][$i] == 0)
                        {
                                move_uploaded_file($_FILES['files']['tmp_name'][$i], $target_dir."/".$name);
                                //echo "File uploaded";
                        }
			$down=1;
                }echo "Folder is successfully uploaded.";
        }else echo "Upload folder is not set.";

/*
if($ok==1)
{
	$dir = "/var/www/html/concrete5/application/uploads/".$foldername;
	//$rootPath = realpath($dir);
	$zip_file = 'file.zip';
	$zip = new ZipArchive();
	$zip->open($zip_file, ZipArchive::CREATE | ZipArchive::OVERWRITE);
	$files = new RecursiveIteratorIterator( new RecursiveDirectoryIterator($dir), RecursiveIteratorIterator::LEAVES_ONLY);

	foreach ($files as $name => $file)
		{
    		// Skip directories (they would be added automatically)
    		if (!$file->isDir())
    			{
        		// Get real and relative path for current file
        		$filePath = $file->getRealPath();
        		$relativePath = substr($filePath, strlen($dir) + 1);
			echo $file;
        		// Add current file to archive
        		$zip->addFile($filePath, $relativePath);
    		}
	}
	$zip->close();

	header('Content-Description: File Transfer');
	header('Content-Type: application/zip');
	header('Content-Disposition: attachment; filename='.basename($zip_file));
	header('Content-Transfer-Encoding: binary');
	header('Expires: 0');
	header('Cache-Control: must-revalidate');
	header('Pragma: public');
	header('Content-Length: ' . filesize($zip_file));
	ob_clean();
	flush();
	readfile($zip_file);

}*/

}
echo "<input type='hidden' name='idProjet' value=".$id_here.">";
//echo "<input type='hidden' name='projetCree' value='chill'>";
echo "<input class='valider' type='submit' name='submit' value='Continuer'>";
echo "</div>";
echo "</form>";

if(isset($_POST['submit'])){
session_start();
//$_POST['idProjet'] = $id_here;
$_SESSION['id'] = $id_here;
$_SESSION['projetCree'] = True;
header('Location: /index.php/lien');
exit();
}
?>


<?php
$view->inc('elements/footer.php');

