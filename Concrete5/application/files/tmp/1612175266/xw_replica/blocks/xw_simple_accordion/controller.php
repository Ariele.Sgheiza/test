<?php

namespace Concrete\Package\XwReplica\Block\XwSimpleAccordion;

use Concrete\Core\Block\BlockController;

class Controller extends BlockController
{
    protected $btTable = 'btXwSimpleAccordion';
    protected $btInterfaceWidth = '700';
    protected $btWrapperClass = 'ccm-ui';
    protected $btInterfaceHeight = '465';

    public function getBlockTypeDescription()
    {
        return t('Add Collapsible Content to your Site');
    }

    public function getBlockTypeName()
    {
        return t('Simple Accordion');
    }

    public function add()
    {
        $this->requireAsset('redactor');
    }

    public function edit()
    {
        $this->requireAsset('redactor');
        $this->requireAsset('javascript', 'bootstrap/collapse');
        $items = $this->getListItems();
        $this->set('items', $items);

    }

    public function view()
    {
        $items = $this->getListItems();
        $this->set('items', $items);
        $this->requireAsset('css', 'font-awesome');
        switch ($this->semantic) {
            case 'h2':
                $openTag = "<h2 class='panel-title'>";
                $closeTag = '</h2>';
                break;
            case 'h3':
                $openTag = "<h3 class='panel-title'>";
                $closeTag = '</h3>';
                break;
            case 'h4':
                $openTag = "<h4 class='panel-title'>";
                $closeTag = '</h4>';
                break;
            case 'p':
                $openTag = "<p class='panel-title'>";
                $closeTag = '</p>';
                break;
            case 'span':
                $openTag = "<span class='panel-title'>";
                $closeTag = '</span>';
                break;
        }
        $this->set('openTag', $openTag);
        $this->set('closeTag', $closeTag);
    }

    public function duplicate($newBID)
    {
        parent::duplicate($newBID);
        $db = $this->app->make('database')->connection();
        $items = $this->getListItems();

        foreach ($items as $row) {
            $queryBuilder = $db->createQueryBuilder()->insert('btXwSimpleAccordionItem')->values([
                'bID' => ':bID', 'title' => ':title', 'description' => ':description',
                'state' => ':state', 'sortOrder' => ':sortOrder',
            ]);
            $queryBuilder->setParameters([
                'bID' => $newBID,
                'title' => $row['title'],
                'description' => $row['description'],
                'state' => $row['state'],
                'sortOrder' => $row['sortOrder'],
            ])->execute();
        }
        $blockObject = $this->getBlockObject();
        if (is_object($blockObject)) {
            $blockObject->setCustomTemplate($this->framework);
        }
    }

    public function delete()
    {
        $db = $this->app->make('database')->connection();
        $queryBuilder = $db->createQueryBuilder();
        $queryBuilder->delete('btXwSimpleAccordionItem')
            ->where($queryBuilder->expr()->eq('bID', ':bID'))
            ->setParameter('bID', $this->bID)
            ->execute();
        parent::delete();
    }

    public function save($args)
    {
        $db = $this->app->make('database')->connection();
        $queryBuilder = $db->createQueryBuilder();
        $queryBuilder->delete('btXwSimpleAccordionItem')
            ->where($queryBuilder->expr()->eq('bID', ':bID'))
            ->setParameter('bID', $this->bID)
            ->execute();
        $count = count($args['sortOrder']);
        $i = 0;
        parent::save($args);
        while ($i < $count) {
            $queryBuilder = $db->createQueryBuilder()->insert('btXwSimpleAccordionItem')->values([
                'bID' => ':bID', 'title' => ':title', 'description' => ':description',
                'state' => ':state', 'sortOrder' => ':sortOrder',
            ]);
            $queryBuilder->setParameters([
                'bID' => $this->bID,
                'title' => $args['title'][$i],
                'description' => $args['description'][$i],
                'state' => $args['state'][$i],
                'sortOrder' => $args['sortOrder'][$i],
            ])->execute();
            ++$i;
        }
        $blockObject = $this->getBlockObject();
        if (is_object($blockObject)) {
            $blockObject->setCustomTemplate($args['framework']);
        }
    }

    public function getListItems()
    {
        $db = $this->app->make('database')->connection();
        $queryBuilder = $db->createQueryBuilder();
        $items = $queryBuilder->select('*')
            ->from('btXwSimpleAccordionItem')
            ->where($queryBuilder->expr()->eq('bID', ':bID'))
            ->setParameter('bID', $this->bID)
            ->orderBy('sortOrder')
            ->execute()
            ->fetchAll();
        return $items;
    }
}
