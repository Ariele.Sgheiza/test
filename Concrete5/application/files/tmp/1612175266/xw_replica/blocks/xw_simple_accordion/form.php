<?php defined('C5_EXECUTE') or die('Access Denied.');

$app = Core::getFacadeApplication();
?>

<style type="text/css">
    .panel-heading { cursor: move; }
    .panel-heading .label-shell { margin-top: 5px; }
    .panel-heading .label-shell label { display: block; text-align: right; }
    .panel-heading .label-shell label i { float: left; margin-top: 3px; cursor: move; }
    .panel-body { display: none; }
    .item-summary { padding: 10px; }
    .item-summary.active { background: #efefef; }
    .item-detail { display: none; background: #efefef; padding: 10px; }
    .tab-pane { padding: 20px 0; }
    .item-shell {position: relative; padding-bottom: 0 !important;  }
    .redactor_editor {  padding: 20px;  }
</style>
<p>
    <?php
    echo $app->make('helper/concrete/ui')->tabs([
        ['pane-items', t('Items'), true],
        ['pane-settings', t('Settings')],
    ]);
    ?>
</p>

<div class="ccm-tab-content" id="ccm-tab-content-pane-items">
    <div class="well">
        <?php  echo t('You can rearrange items if needed.'); ?>
    </div>

    <div class="items-container"></div>

    <span class="btn btn-success btn-add-item"><?php  echo t('Add Item'); ?></span>
</div>

<div class="ccm-tab-content" id="ccm-tab-content-pane-settings">

    <div class="form-group">
        <label class="form-label"><?php  echo t('Use Framework Markup'); ?></label>
        <div class="well">
            <?php  echo t('If your theme uses the bootstrap framework, then select that. Otherwise, just choose none'); ?>
        </div>
        <?php  echo $form->select('framework', ['' => t('None'), 'bootstrap' => t('Bootstrap')], $framework); ?>
    </div>
    <div class="form-group">
        <label class="form-label"><?php  echo t('Semantic Tag for Title'); ?></label>
        <?php  echo $form->select('semantic', ['h2' => t('Heading %d', 2), 'h3' => t('Heading %d', 3), 'h4' => t('Heading %d', 4), 'p' => t('Paragraph'), 'span' => tc('HTML Element', 'Span')], $semantic); ?>
    </div>
</div>

<script type="text/template" id="item-template">
    <div class="item panel panel-default" data-order="<%=sort%>">
        <div class="panel-heading">
            <div class="row">
                <div class="col-xs-3 label-shell">
                    <label for="title<%=sort%>"><i class="fa fa-arrows drag-handle"></i> <?php echo t('Title'); ?></label>
                </div>
                <div class="col-xs-5">
                    <input type="text" class="form-control" name="title[]" value="<%=title%>">
                </div>
                <div class="col-xs-4 text-right">
                    <a href="javascript:XwAccordionForm.editItem(<%-sort%>)" class="btn btn-edit-item btn-default"><?php echo t('Edit'); ?></a>
                    <a href="javascript:XwAccordionForm.deleteItem(<%-sort%>)" class="btn btn-delete-item btn-danger"><?php echo t('Delete'); ?></a>
                </div>
            </div>
        </div>
        <div class="panel-body form-horizontal">
            <div class="form-group">
                <label class="col-xs-3 control-label" for="description<%=sort%>"><?php echo t('Description:'); ?></label>
                <div class="col-xs-9">
                    <textarea class="editor-content" name="description[]" id="description<%=sort%>"><%=description%></textarea>
                </div>
            </div>
            <div class="form-group">
                <label class="col-xs-3 control-label"><?php echo t('State'); ?></label>
                <div class="col-xs-9">
                    <select class="form-control" name="state[]">
                        <option value="closed" <%= state=='closed' ? 'selected' : '' %>><?php  echo t('Closed'); ?></option>
                        <option value="open" <%= state=='open' ? 'selected' : '' %>><?php  echo t('Open'); ?></option>
                    </select>
                </div>
            </div>
        </div>
        <input class="item-sort" type="hidden" name="<?php  echo $view->field('sortOrder'); ?>[]" value="<%=sort%>"/>
    </div>
</script>

<script type="text/javascript">
    var CCM_EDITOR_SECURITY_TOKEN = "<?php echo $app['token']->generate('editor'); ?>";
    var XwAccordionForm = {
        editItem: function(i) {
            $(".item[data-order='"+i+"']").find(".panel-body").toggle();
        },
        deleteItem: function(i) {
            if (confirm('<?php echo t('Are you sure?'); ?>')) {
                var itemID = $(".item[data-order='"+i+"']").find('.editor-content').attr('id');
                if (typeof CKEDITOR === 'object') {
                    CKEDITOR.instances[itemID].destroy();
                }

                $(".item[data-order='"+i+"']").remove();
                this.indexItems();
            }
        },
        indexItems: function() {
            $('.items-container .item').each(function(i) {
                $(this).find('.item-sort').val(i);
                $(this).attr("data-order", i);
            });
        },
        launchEditor: <?php echo $app['editor']->outputStandardEditorInitJSFunction(); ?>
    };

    $(function(){
        var itemsContainer = $('.items-container');
        var itemTemplate = _.template($('#item-template').html());

        //Make items sortable. If we re-sort them, re-index them.
        $(".items-container").sortable({
            handle: ".panel-heading",
            update: function(){
                XwAccordionForm.indexItems();
            }
        });

        //LOAD UP OUR ITEMS

        //for each Item, apply the template.
        <?php
        if ($items) {
        foreach ($items as $item) {
        ?>
        itemsContainer.append(itemTemplate({
            //define variables to pass to the template.
            title: '<?php  echo addslashes($item['title']); ?>',
            description: '<?php  echo str_replace(["\t", "\r", "\n"], '', addslashes($item['description'])); ?>',
            state: '<?php echo $item['state']; ?>',
            sort: '<?php echo $item['sortOrder']; ?>'
        }));
        <?php
        }
        }
        ?>

        //Init Index
        XwAccordionForm.indexItems();

        //Init Editor
        if ($('.editor-content').length) {
            XwAccordionForm.launchEditor($('.editor-content'));
        }

        //CREATE NEW ITEM
        $('.btn-add-item').click(function(){
            //Use the template to create a new item.
            var temp = $(".items-container .item").length;
            temp = (temp);
            itemsContainer.append(itemTemplate({
                //vars to pass to the template
                title: '',
                description: '',
                state: '',
                sort: temp
            }));

            var thisModal = $(this).closest('.ui-dialog-content');
            var newItem = $('.items-container .item').last();
            thisModal.scrollTop(newItem.offset().top);

            //Init Editor
            XwAccordionForm.launchEditor(newItem.find('.editor-content'))

            //Init Index
            XwAccordionForm.indexItems();
        });
    });
</script>
