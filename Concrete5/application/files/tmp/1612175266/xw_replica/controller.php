<?php
namespace Concrete\Package\XwReplica;

use Concrete\Core\Backup\ContentImporter\Importer\Routine\ImportPageTypesBaseRoutine as CoreImportPageTypesBaseRoutine;
use Concrete\Core\Block\BlockType\BlockType;
use Concrete\Core\Database\EntityManager\Provider\ProviderInterface;
use Concrete\Core\Package\Package;
use Concrete\Package\XwReplica\Backup\ContentImporter\Importer\Routine\ImportPageTypesBaseRoutine;
use Concrete\Package\XwReplica\Theme\XwReplica\PageTheme;

class Controller extends Package implements ProviderInterface
{
    protected $pkgHandle = 'xw_replica';
    protected $appVersionRequired = '8.5.0';
    protected $pkgVersion = '1.0.0';
    protected $pkgAutoloaderRegistries = [
        'src' => 'Concrete\Package\XwReplica',
    ];

    public function getPackageName()
    {
        return t('Replica Theme');
    }

    public function getPackageDescription()
    {
        return t('Replica Theme, by Xanweb');
    }

    public function install()
    {
        $pkg = parent::install();
        $this->app->bind(CoreImportPageTypesBaseRoutine::class, ImportPageTypesBaseRoutine::class);
        $theme = PageTheme::add('xw_replica', $pkg);
        $theme->applyToSite();

        $this->installXml();

        $this->installComponents($pkg);

        return $pkg;
    }

    public function upgrade()
    {
        parent::upgrade();
        $this->installComponents($this->getPackageEntity());
    }

    /**
     * Install or Update package components.
     *
     * @param \Concrete\Core\Entity\Package $pkg
     */
    private function installComponents($pkg)
    {
        $this->installBlockTypes(['xw_simple_accordion', 'xw_quick_tabs'], $pkg);
    }

    /**
     * Install/update data from install XML file.
     */
    private function installXml()
    {
        $this->installContentFile('install.xml');
    }

    private function installBlockTypes(array $blockTypes, $pkg)
    {
        foreach ($blockTypes as $handle) {
            $bt = BlockType::getByHandle($handle);
            if (!is_object($bt)) {
                $bt = BlockType::installBlockType($handle, $pkg);
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getDrivers()
    {
        return [];
    }
}
