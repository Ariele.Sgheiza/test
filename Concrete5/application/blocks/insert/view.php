<?php
use Concrete\Core\Database\Connection;
use Concrete\Core\Database\Driver\PDOStatement;

defined('C5_EXECUTE') or die(_("Access Denied."))

?>
/*<?php
require_once 'application/init.php';

if(!empty($_POST)){
	if(isset($_POST['titre'], $_POST['desc'], $_POST['cle'] )){
	$titre= $_POST['titre'];
	$desc= $_POST['desc'];
	$cle= explode(',' , $_POST['cle']);
	$indexed = $es-> index([
		'index' => 'projets',
		'type' => 'projet',
		'body' => [
			'titre' => $titre,
			'desc' => $desc,
			'cle' => $cle

		]

	]);
	if ($indexed){
		print_r($indexed);
	}

	}
}
?>*/
<form class='input' method="post" enctype="multipart/form-data">
<div class='div1'>
<input class = 'putTitre' type="text" name="titre" placeholder="Nom de votre projet" id="insert" value="<?php echo $titre ?>">

<input class='putDesc' type="text" name="desc" placeholder="Description projet" id="insert" value="<?php echo $desc?>">

<input class='putCle' type="text" name="cle" placeholder="Mot cle" id="insert" value="<?php echo $cle?>">
 </div>
 <div class='div2'>
<select class='selCours' name="choisir">
<?php
$db = mysqli_connect('localhost', 'cui', 'portfoliounige', 'portfolio');
$sql = "SELECT nom_cours FROM Cours";
$res = mysqli_query($db, $sql);
while($rows = $res->fetch_assoc())
{
	$nom_cours = $rows['nom_cours'];
	echo "<option value='$nom_cours'>$nom_cours</option>";
}
?>
</select>

<div>
<select class='selCat'  name="categorie">
<?php
$db1 = mysqli_connect('localhost', 'cui', 'portfoliounige', 'portfolio');
$sql1 = "SELECT nom_cat FROM Categorie";
$res = mysqli_query($db1, $sql1);
while($rows = $res->fetch_assoc())
{
        $nom_cat = $rows['nom_cat'];
        echo "<option value='$nom_cat'>$nom_cat</option>";
}
?>
</select>
</div>
</div>
<p class='selCat'>
Autoriser le téléchargement ?
<input class='dwld' type="checkbox" name="download" <?php if (isset($download) && $download=="Oui") echo "checked";?> value="oui">Oui
</p>
 <div class='div4'>
<!-- <form action="upload.php" method="post" enctype="multipart/form-data"> -->
	Créer un dossier :
<!--	<input type="text" name="foldername"/> -->
	<input class='file' type="file" name="files[]" id="files" multiple directory="" webkitdirectory="" mozdirectory="">
<!-- 	<input type="submit" value="Upload File" name="submit"> -->
<!-- </form> -->
</div>
<input class='valider'  type="submit" name="submit" value="Submit">

</form>

<?php

if(isset($_POST['submit'])){
	//$filename = $_FILES["files"]["name"];

	$ok=0;
	$titre = $_POST['titre'];
	$desc = $_POST['desc'];
	$cle = $_POST['cle'];
	$down = $_POST['download'];
	if($down == "oui") $ok = 1;
	$cours = $_POST['choisir'];
	$cat = $_POST['categorie'];
	$db = \Database::connection();
	$id=$db->fetchColumn("SELECT id_cours FROM Cours WHERE nom_cours=?", [$cours]);
	if($id=='') $id='D200018';
	$db->query("INSERT INTO Projet (titre_projet, desc_projet, mot_cle, telecharger, id_cours) VALUES (?,?,?,?,?)", [$titre,$desc,$cle,$ok,$id]);
	$id_cat = $db->fetchColumn("SELECT id_cat FROM Categorie WHERE nom_cat=?", [$cat]);
	$id_here = $db->fetchColumn("SELECT id_projet FROM Projet WHERE titre_projet=?",[$titre]);
	$db->query("INSERT INTO ProjetCategorie (id_projet, id_cat) VALUES (?,?)", [$id_here, $id_cat]);
	$db->query("INSERT INTO EtudiantProjet (id_etu, id_projet) VALUES (?,?)", ["1", $id_here]);
if(isset($id_here)){
//		$down=0;
                $foldername = $id_here;
                if(!is_dir($foldername)) mkdir($foldername);
                rename("/var/www/html/concrete5/".$foldername , "/var/www/html/concrete5/application/uploads/".$foldername);
                $target_dir = "/var/www/html/concrete5/application/uploads/".$foldername;

                foreach ($_FILES['files']['name'] as $i => $name)
                {
                        if($_FILES['files']['name'][$i] == 0)
                        {
                                move_uploaded_file($_FILES['files']['tmp_name'][$i], $target_dir."/".$name);
                                //echo "File uploaded";
                        }
			$down=1;
                }echo "Folder is successfully uploaded.";
        }else echo "Upload folder is not set.";

if($_POST['download']=="oui")
{
	$dir = "/var/www/html/concrete5/application/uploads/".$foldername;
	//$rootPath = realpath($dir);
	$zip_file = 'file.zip';
	$zip = new ZipArchive();
	$zip->open($zip_file, ZipArchive::CREATE | ZipArchive::OVERWRITE);
	$files = new RecursiveIteratorIterator( new RecursiveDirectoryIterator($dir), RecursiveIteratorIterator::LEAVES_ONLY);

	foreach ($files as $name => $file)
		{
    		if (!$file->isDir())
    			{
        		$filePath = $file->getRealPath();
        		$relativePath = substr($filePath, strlen($dir) + 1);
			echo $file;

        		$zip->addFile($filePath, $relativePath);
    		}
	}
	$zip->close();

	header('Content-Description: File Transfer');
	header('Content-Type: application/zip');
	header('Content-Disposition: attachment; filename='.basename($zip_file));
	header('Content-Transfer-Encoding: binary');
	header('Expires: 0');
	header('Cache-Control: must-revalidate');
	header('Pragma: public');
	header('Content-Length: ' . filesize($zip_file));
	ob_clean();
	flush();
	readfile($zip_file);

}
}
?>

