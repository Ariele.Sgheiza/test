<?php

namespace Application\Block\Insert;

use Concrete\Core\Block\BlockController;
use Core;

defined('C5_EXECUTE') or die(_("Access Denied."));

class Controller extends BlockController
{

    protected $btTable = "btInsert";
    protected $btInterfaceWidth = "350";
    protected $btInterfaceHeight = "240";
    protected $btDefaultSet = 'basic';

    public function getBlockTypeName()
    {
        return t('Insert');
    }

    public function getBlockTypeDescription()
    {
        return t('A simple starting block for developers');
    }
}
