<?php 
use Concrete\Core\Database\Connection;

defined('C5_EXECUTE') or die(_("Access Denied."))

?>

<div>
    <div><?php echo t('Field 1')?></div>
    <div><?php  
    $db=\Database::connection();
    echo $titre = $db->fetchColumn('SELECT titre_projet FROM Projet WHERE id_projet=?',[$field1]);
?>
</div>
</div>

<div>
    <div><?php echo t('Field 2')?></div>
    <div><?php echo $field2 ?></div>
</div>

<div>
    <div><?php echo t('Boolean')?></div>
    <div>
    <?php if ($booleanfield) { ?>
        <?php echo t('Yes')?>
    <?php } else { ?>
        <?php echo t('No')?>
    <?php } ?>
    </div>
</div>

