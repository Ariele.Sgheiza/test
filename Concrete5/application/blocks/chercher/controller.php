<?php

namespace Application\Block\Chercher;

use Concrete\Core\Block\BlockController;
use Core;
use Concrete\Core\Database\Connection;

defined('C5_EXECUTE') or die(_("Access Denied."));

class Controller extends BlockController
{

    protected $btTable = "btchercher";
    protected $btInterfaceWidth = "350";
    protected $btInterfaceHeight = "240";
    protected $btDefaultSet = 'basic';

    public function getBlockTypeName()
    {
        return t('chercher');
    }

    public function getBlockTypeDescription()
    {
        return t('chercher un projet');
    }
}


function on_start()
{
    $al = \Concrete\Core\Asset\AssetList::getInstance();
    $al->register(
        'css', 'cherche', 'blocks/chercher/cherche.min.css',
        array('version' => '2.16.3', 'minify' => false, 'combine' => true)
    );
$al->registerGroup('cherche', array(
    array('css', 'cherche'),
));


}



function registerViewAssets()
{
    $this->requireAsset('cherche');
}
