<?php
use Concrete\Core\Database\Connection;
defined('C5_EXECUTE') or die(_("Access Denied."))
?>
<?php /*
require_once 'application/init.php';
if (isset($_POST['recherche'])){

    $recherche=$_POST['recherche'];
    $query=$es ->search([
        'body'=> [
            'query' =>[
                'bool'=>[
                    'should'=>[
                        'match'=>['title'=>$recherche],
                        'match'=>['desc'=>$recherche],
                        'match'=>['cle'=> $recherche]

                    ]
                ]
            ]
        ]
    ]);
    if ($query['hits']['total']>=1){
        $results=$query['hits']['hits'];
    }

}*/
?>
<style>
	#formCherche{
	    position: absolute;
      	    margin-top:20px;
            width: 460px;
            margin-left: 38%;
            margin-right: 38%;
            padding-top: 10px;
        }

        input[type='text']{
            border-radius: 10px;
            width: 400px;
            height: 40px;
	    border: 2px solid #7D003C;
	    border-right: none;
            border-top-right-radius:0px;
	   border-bottom-right-radius:0px;
	}

        #valide{
            border-radius: 10px;
            width: 40px;
            height: 42px;
            border: 2px solid #7D003C;
            background-color: #7D003C;
            font-family: Arial, Helvetica, sans-serif;
            color: white;
            font-weight: bold;
            font-size: large;
	    border-left:none;
	    border-top-left-radius:0px;
	    border-bottom-left-radius:0px;
	    padding-left:2px;
        }

        #valide:hover{
            cursor: pointer;
        }

        *:focus{
            outline: none;
        }

	#dropDownNuage{
	position: relative;
	display:flex;
	margin-top:15px;
	left:-20%;
	}

	.selCours{
	border-radius:10px;
	padding:3px;
	background-color:#7D003C;
	color:white;
	border: 2px solid #7D003C;
	text-align:center;
	margin-left:5px;
	}

	.selCours:hover{
	cursor:pointer;
	}

	#chercher{
	display:flex;
	}
    </style>
<?php
$layout_dir = $this->getBlockPath() . '/loupe.png';
?>
<form id="formCherche" method="post" action="/index.php/recherche">
<div id="chercher">
<input type="text" name = "recherche" placeholder="Cherchez un projet..." id="search" value = "<?php echo $recherche?>" >
<input id="valide"  type="image" name="submit" src="/application/blocks/chercher/loupe.png" width="15px" height="15px">
</div>
<div id="dropDownNuage">

<select class='selCours' name="catCheck">
<?php
$db = mysqli_connect('localhost', 'cui', 'portfoliounige', 'portfolio');
$sql = "SELECT id_theme,nom_theme FROM Theme";
$res = mysqli_query($db, $sql);
if(isset($_POST['catSearchProjet'])){
echo "<option value='".$_POST['catSearchProjet']."'>".$_POST['catSearchProjet']."</option>";
unset($_POST['catSearchProjet']);
}
echo "<option value='theme'>Sélection theme</option>";

while($rows = $res->fetch_assoc())
{
        $nom_theme = $rows['nom_theme'];
	$id_theme = $rows['id_theme'];
        if($_SESSION['cateSearchProjet']!=$nom_theme){
		echo "<option value='$nom_theme'>$nom_theme</option>";
	}
}
?>
</select>

<select class='selCours' name="choisirCours">
<?php
$db = mysqli_connect('localhost', 'cui', 'portfoliounige', 'portfolio');
$sql1 = "SELECT id_cours, nom_cours FROM Cours";
$res = mysqli_query($db, $sql1);
echo "<option value='cours'>Sélection cours</option>";
while($rows = $res->fetch_assoc())
{
        $nom_cours = $rows['nom_cours'];
	$id_cours = $rows['id_cours'];
        echo "<option value='$id_cours'>$nom_cours</option>";
}
?>
</select>

<select class='selCours' name="typeCheck">
<?php
$db = mysqli_connect('localhost', 'cui', 'portfoliounige', 'portfolio');
$sql2 = "SELECT id_type, nom_type FROM Type";
$res = mysqli_query($db, $sql2);
if(isset($_POST['typeSearchProjet'])){
echo "<option value='".$_POST['typeCheck']."'>".$_POST['typeSearchProjet']."</option>";
}
echo "<option value='type'>Sélection type</option>";
while($rows = $res->fetch_assoc())
{
        $nom_type = $rows['nom_type'];
	$id_type = $rows['id_type'];
        echo "<option value='$id_type'>$nom_type</option>";
}
?>
</select>

<select class='selCours' name="choisirProf">
<?php
$array = array();
$db = mysqli_connect('localhost', 'cui', 'portfoliounige', 'portfolio');
$sql3 = "SELECT prof_cours FROM Cours";
$res = mysqli_query($db, $sql3);
echo "<option value='prof'>Sélection professeur</option>";
while($rows = $res->fetch_assoc())
{
	if(!in_array($rows['prof_cours'], $array)){
                $array[] = $rows['prof_cours'];
        	$prof_cours = $rows['prof_cours'];
        	echo "<option value='$prof_cours'>$prof_cours</option>";
	}
}
?>
</select>

</div>
</form>

<?php
	$res = $_POST['recherche'];
	$db=\Database::connection();
	$titre = $db->fetchColumn('SELECT resume FROM Projet WHERE titre_projet=?',[$res]);
	//echo $titre;
?>

