<?php

/**
 * -----------------------------------------------------------------------------
 * Generated 2021-08-27T17:16:33+02:00
 *
 * DO NOT EDIT THIS FILE DIRECTLY
 *
 * @item      misc.do_page_reindex_check
 * @group     concrete
 * @namespace null
 * -----------------------------------------------------------------------------
 */
return [
    'version_installed' => '8.5.4',
    'version_db_installed' => '20200609145307',
    'misc' => [
        'login_redirect' => 'DESKTOP',
        'access_entity_updated' => 1612014699,
        'latest_version' => '8.5.4',
        'do_page_reindex_check' => false,
        'mobile_theme_id' => 0,
    ],
];
