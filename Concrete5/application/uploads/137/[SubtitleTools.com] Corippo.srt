﻿1
00:03:26,000 --> 00:03:32,000
Je vis à Corippo

2
00:03:47,000 --> 00:03:48,000
Salut !

3
00:03:49,000 --> 00:03:50,500
Je suis Gloria.

4
00:03:51,000 --> 00:03:52,500
Bon travail !

5
00:03:53,000 --> 00:03:54,500
On y va ?

6
00:03:54,500 --> 00:03:57,000
- Où est mamie ?
- Elle est déjà là.

7
00:03:57,000 --> 00:03:58,000
Aah !

8
00:04:13,000 --> 00:04:15,000
-C'est fermé.
-C'est fermé ?!

9
00:04:15,000 --> 00:04:18,000
-Essaies toi.
-C'est étrange eh.

10
00:04:22,000 --> 00:04:24,000
Samedi 14...

11
00:04:24,500 --> 00:04:26,000
Ah non, ça c'est Novembre.

12
00:04:26,500 --> 00:04:30,000
En Décembre ils ne font rien.

13
00:04:40,000 --> 00:04:48,000
Chers habitants, à cause de la pandémie l'arrivée du Père Noël est annulée.
- Le Père Noël ne viendra pas ici!

14
00:04:57,000 --> 00:04:59,000
Viens ici mamie.

15
00:05:05,000 --> 00:05:07,000
Attention eh.

16
00:05:12,000 --> 00:05:16,000
-ça sera la nouvelle Corippo.
-Elle est où ta maison ?

17
00:05:17,000 --> 00:05:19,000
Ici il y a l'église donc...

18
00:05:22,000 --> 00:05:24,000
Celle-là non?

19
00:05:24,000 --> 00:05:28,000
On monte les escaliers et c'est celle-là.

20
00:05:33,000 --> 00:05:34,500
Regarde les chambres.

21
00:05:34,500 --> 00:05:37,000
-Les chambres...
-Sont comme ça.

22
00:05:38,000 --> 00:05:41,000
Attends, ça... ah oui c'est en italien.

23
00:05:42,000 --> 00:05:46,000
-Je le veux en italien.
-Oui c'est juste.

24
00:05:47,000 --> 00:05:49,500
Maintenant ils sont en train de construire ça,

25
00:05:49,500 --> 00:05:53,000
et ce sont les maisons qui deviendront des chambres.

26
00:05:55,000 --> 00:05:56,500
Bâtiments étape 1.

27
00:06:07,000 --> 00:06:10,000
Il sonne une minute avant et une minute plus tard.

28
00:06:10,000 --> 00:06:11,500
Il ne sonne jamais juste !

29
00:06:27,000 --> 00:06:31,000
Comment ça va ? Tout va bien ?

30
00:06:31,000 --> 00:06:33,000
Couci-couça !

31
00:06:34,000 --> 00:06:38,500
-Tu connais Valentino ?
-Oui, nous nous sommes déjà rencontrés quand je suis venu à la fête du village.

32
00:06:38,500 --> 00:06:40,000
-On y va ?
-On y va !

33
00:06:56,000 --> 00:06:58,000
Ça va ?

34
00:06:58,000 --> 00:06:59,000
Fatiguée !

35
00:07:03,000 --> 00:07:08,000
Fatiguée, encore plus avec ces masques.

36
00:07:14,500 --> 00:07:16,000
-Eh alors !

37
00:07:16,000 --> 00:07:19,000
-Tu es encore à l'école toi ?
-Oui encore à l'école.

38
00:07:19,000 --> 00:07:26,000
-C'est le moment d'arrêter !
-D'arrêter et de venir habiter à Corippo, eh Valentino !

39
00:07:26,000 --> 00:07:29,500
J'ai arrêté de travailler dès que j'ai eu l'âge de la retraite.

40
00:07:29,500 --> 00:07:31,000
Les jeunes doivent continuer!

41
00:07:31,500 --> 00:07:35,000
-Mais toi t'es né ici, à Corippo ?
-Oui oui.

42
00:07:35,000 --> 00:07:42,000
Mais on avait des biens à Quartino: de la terre, une ferme...

43
00:07:42,000 --> 00:07:47,000
Et tu aimes vivre à Corippo ?

44
00:07:47,000 --> 00:07:49,000
-Maintenant oui !
-C'est l'habitude !

45
00:07:49,000 --> 00:07:52,500
T'as jamais pensé de vivre à Quartino ?

46
00:07:53,000 --> 00:07:57,000
Eh, puis j'avais une copine donc !

47
00:07:57,000 --> 00:07:58,500
Et t'as choisi Corippo.

48
00:07:58,500 --> 00:08:03,000
-Mais ils t'ont forcé ?
-Non non.

49
00:08:03,000 --> 00:08:05,000
Je suis resté pour ma mère.

50
00:08:06,000 --> 00:08:10,000
-Et maintenant avec ces travaux, Valentino ?
-Ne m'en parlez pas !

51
00:08:10,000 --> 00:08:15,000
Ils mangent les bêtes ! Tous les jeudis qu'ils viennent ici, ils mangent !

52
00:08:15,000 --> 00:08:17,000
T'es pas content ?

53
00:08:18,000 --> 00:08:23,000
C'est pas beau. C'était mieux avant.

54
00:08:23,000 --> 00:08:29,000
Maintenant ils ont tout détruit. Au moins 4 ans avant qu'ils finissent tout.

55
00:08:29,500 --> 00:08:36,000
Oui! Car maintenant ils travaillent encore une semaine puis ils arrêtent pour Noël.

56
00:08:36,000 --> 00:08:39,000
Puis pendant un mois ils ne travailleront pas.

57
00:08:39,000 --> 00:08:43,500
On verra si arrive la neige, il ne pourront pas travailler.

58
00:08:43,500 --> 00:08:45,000
Il sera pas bien alors ?

59
00:08:45,000 --> 00:08:48,000
Non, pour moi, ils doivent l'annuler.

60
00:08:48,500 --> 00:08:51,000
Pourquoi tu dis ça ?

61
00:08:51,000 --> 00:08:59,000
Avant, il y avait une grande terrasse, maintenant ce sera seulement du verre et un mur.

62
00:08:59,000 --> 00:09:01,000
-Il n'y a plus la terrasse ?
-Non non.

63
00:09:01,000 --> 00:09:09,000
Ils doivent encore organiser les places de parking, l'eau et les égouts.

64
00:09:09,000 --> 00:09:13,000
L'eau est un problème en été.

65
00:09:14,000 --> 00:09:16,000
Et l'hôtel ? Sera comment ?

66
00:09:16,500 --> 00:09:20,000
Et puis ils mettront les chambres à l'étage.

67
00:09:20,500 --> 00:09:22,500
Et puis il y aura beaucoup de touristes!

68
00:09:23,000 --> 00:09:26,000
-Il y aura des filles ici !
-Les filles !

69
00:09:28,000 --> 00:09:30,000
T'es content alors !

70
00:09:32,000 --> 00:09:33,000
On verra !

71
00:09:35,500 --> 00:09:37,000
Une chose positive !

72
00:09:39,500 --> 00:09:40,500
Est-ce qu'ils mettent le bazar ?

73
00:09:41,000 --> 00:09:44,000
Parfois, quand arrivent les jeunes.

74
00:09:44,000 --> 00:09:46,000
Ils boivent un peu et puis...

75
00:09:50,000 --> 00:09:54,500
Je dois les gronder un peu!

76
00:09:55,000 --> 00:10:00,000
-Maintenant combien des gens vivent à Corippo ?
-Nous sommes que 3 !

77
00:10:00,000 --> 00:10:01,000
Toi et qui ?

78
00:10:01,000 --> 00:10:08,000
Moi... Claudio et Carolina.

79
00:10:08,000 --> 00:10:10,500
Les autres sont à la maison de retraite.

80
00:10:11,000 --> 00:10:12,500
-Et Guglielmo?
-Aussi.

81
00:10:13,000 --> 00:10:15,500
-C'est mieux...
-À Gordola ?

82
00:10:15,500 --> 00:10:19,000
-Oui oui. C'est mieux.
-Pauvres !

83
00:10:19,500 --> 00:10:28,000
-Donc vous avez quel âge ?
-C'est moi, Claudio, Carolina et les allemands qui arrivent.

84
00:10:28,000 --> 00:10:32,000
-Mais ils viennent en vacances ?
-Non non, ils ont le domicile.

85
00:10:32,500 --> 00:10:36,000
-Oui, ils sont plus ou moins 20.
-Ah, 20 !

86
00:10:36,500 --> 00:10:39,000
Oui, si on compte tous les allemands.

87
00:10:39,000 --> 00:10:40,500
Ils viennent ici souvent ?

88
00:10:40,500 --> 00:10:43,000
Ils ont le domicile.

89
00:10:43,000 --> 00:10:46,000
Mais ils sont pas toujours ici non ? Seulement le weekend ?

90
00:10:46,000 --> 00:10:48,000
Non non toujours ! Toute l'année.

91
00:10:48,000 --> 00:10:52,000
Un juste ici et un autre sur la montagne.

92
00:10:53,000 --> 00:10:56,000
-Donc ils sont à la retraite ?
-Non non, ils travaillent aussi.

93
00:11:00,000 --> 00:11:04,000
Ah non, un est à la retraite.

94
00:11:05,000 --> 00:11:07,000
Vous vous voyez quelques fois?

95
00:11:07,000 --> 00:11:12,000
Oui quelque fois, quand ils ont besoin de quelque chose ils me demandent.

96
00:11:13,500 --> 00:11:16,000
Mais en été il y a plus de gens ?

97
00:11:17,000 --> 00:11:19,000
Ne m'en parlez pas !

98
00:11:19,000 --> 00:11:24,000
Mais dans tout la vallée, c'était pire que d'aller à la mer.

99
00:11:24,500 --> 00:11:31,500
Mais avec le virus ils ne peuvent pas aller à l'étranger...

100
00:11:32,000 --> 00:11:38,000
T'es toujours ici à contrôler. Ta maison est en première ligne pour contrôler.

101
00:11:39,000 --> 00:11:42,000
-Je les fais partir tout de suite !
-Tu fais comment ?!

102
00:11:42,500 --> 00:11:46,000
Je dis : "vous devez partir !"

103
00:11:46,000 --> 00:11:48,000
Tu n'es pas accueillant !

104
00:11:48,000 --> 00:11:56,000
Non ! Aussi à la télévision je dis de partir ! Vous avez fait assez de la publicité ! Arrêtez !

105
00:11:58,000 --> 00:12:04,000
Mais c'est vrai, ils continuent à faire de la publicité, ils n'arrêtent pas.

106
00:12:08,000 --> 00:12:09,500
T'as quel âge Valentino ? Je ne me rappelle plus.

107
00:12:09,500 --> 00:12:12,000
Je suis né en 1944, fait les calculs.

108
00:12:12,500 --> 00:12:15,000
On fait pas les calculs ! On ne peut pas le dire !

109
00:12:16,000 --> 00:12:20,000
-Et toi ? T'a quel âge? Dis à Valentino.
-Il le sait déjà.

110
00:12:20,000 --> 00:12:23,000
-Tu te rappelles ? Elle a 94 ans!

111
00:12:25,000 --> 00:12:31,500
Quand je vous ai connu, il y avait une belle harmonie.

112
00:12:32,000 --> 00:12:37,000
-Ton père et ta mère.
-Ton père était maire, pas vrai ?

113
00:12:37,000 --> 00:12:39,000
-Oui oui.
-Pendant combien de temps ?

114
00:12:40,000 --> 00:12:43,000
Beaucoup. Plus que 20 ans.

115
00:12:43,500 --> 00:12:46,000
-Et tu continues à contrôler.
-Tout est toujours sous contrôle.

116
00:12:46,500 --> 00:12:48,000
Contrôle municipal !

117
00:12:49,500 --> 00:12:53,000
-T'as des bons souvenirs de Corippo?
-Oui beaucoup de souvenirs !

118
00:12:55,000 --> 00:13:05,500
Puis il y avait Père Marcello qui faisait la messe le samedi soir pour rester manger ici !

119
00:13:06,000 --> 00:13:08,000
Des belles soirées, Valentino !

120
00:13:08,000 --> 00:13:10,000
Il était très gentil.

121
00:13:10,000 --> 00:13:14,500
-Il était si content, et nous le gardions comme...
-Il était vraiment bien.

122
00:13:15,000 --> 00:13:16,000
Vraiment bien.

123
00:13:17,000 --> 00:13:20,500
Oui, j'ai des bons souvenirs, je suis attachée à Corippo.

124
00:13:20,500 --> 00:13:22,500
Pendant combien de temps n'es-tu pas revenu à Corippo?

125
00:13:23,000 --> 00:13:31,000
Je me suis mariée en 1954... Puis pendant 20 ans on n'est plus venu ici

126
00:13:31,000 --> 00:13:36,000
Puis nous avons commencé à venir, pendant l'été.

127
00:13:36,000 --> 00:13:38,500
Voilà ! C'est comme ça !

128
00:13:38,500 --> 00:13:44,000
-Puis tu fait ta journée...
-Oui, je fais des promenades.

129
00:13:44,000 --> 00:13:47,000
Maintenant il fait froid.

130
00:13:47,000 --> 00:13:52,000
Des gens m'appellent pour faire des choses mais maintenant, j'arrête!

131
00:13:53,000 --> 00:13:54,500
Merci Valentino.

132
00:13:57,000 --> 00:13:59,500
Comme ça on entend l'histoire.

133
00:14:00,000 --> 00:14:05,000
Comme on vivait dans le passé. C'est important l'histoire.

134
00:14:08,000 --> 00:14:09,000
Merci Valentino !

135
00:14:27,000 --> 00:14:29,000
Salut Valentino !

136
00:14:29,500 --> 00:14:32,000
On va à l'étage pour voir comment ça a changé ?

137
00:14:38,000 --> 00:14:40,000
Il y a déjà la neige.

138
00:14:44,000 --> 00:14:46,500
C'est la plus belle, cette chambre.

139
00:14:51,000 --> 00:14:53,000
Ça te manque ?
Oui ?

140
00:14:53,000 --> 00:14:57,500
Oui, car on l'utilisait souvent.

141
00:15:00,000 --> 00:15:05,000
Puis après le décès de Luciano, les choses ont changé.

142
00:15:05,000 --> 00:15:06,000
Malheureusement.

143
00:15:47,000 --> 00:15:51,000
Cette fontaine on l'a jamais utilisée.

144
00:16:00,000 --> 00:16:04,000
-C'est magnifique !
-Voilà !

145
00:16:04,000 --> 00:16:10,000
-Je le mettrais dans l'entrée ! Merci !
-Je l'ai cueilli ce matin.

146
00:16:10,000 --> 00:16:15,000
-Merci Valentino ! Quel cadeau !
-C'est un peut petit!

147
00:16:19,000 --> 00:16:22,000
Je le laisse ici pour l'instant. Merci beaucoup !

148
00:16:27,000 --> 00:16:29,000
Rappelez moi de le prendre.

149
00:16:30,000 --> 00:16:33,500
C'est le symbole de Noël.

150
00:16:53,000 --> 00:16:55,000
Je prend ton sac ?

151
00:17:03,000 --> 00:17:05,000
On peut aller dans la petite rue là-bas.

152
00:18:33,000 --> 00:18:35,500
Quel discours feriez-vous aujourd'hui, monsieur le maire ?

153
00:18:36,000 --> 00:18:43,500
Le discours pour mes villageois c'est de résister pour Corippo ! Comme moi !

154
00:18:43,500 --> 00:18:46,500
Mais pour eux c'est difficile à comprendre.

155
00:18:48,000 --> 00:18:53,500
Les jeunes aujourd'hui n'écoutent pas.

156
00:18:55,000 --> 00:19:02,000
Chacun fait sa journée, et on verra combien de temps durera cette journée.
