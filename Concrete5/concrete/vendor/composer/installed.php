<?php return array (
  'root' => 
  array (
    'pretty_version' => '1.0.0+no-version-set',
    'version' => '1.0.0.0',
    'aliases' => 
    array (
    ),
    'reference' => NULL,
    'name' => 'concrete5/core',
  ),
  'versions' => 
  array (
    'anahkiasen/html-object' => 
    array (
      'pretty_version' => '1.4.4',
      'version' => '1.4.4.0',
      'aliases' => 
      array (
      ),
      'reference' => '45bb54b91112c064d3906c207259d5c8dcba798f',
    ),
    'commerceguys/addressing' => 
    array (
      'pretty_version' => 'v1.1.1',
      'version' => '1.1.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '6df5c0eea9d1f370095585eef1b08f4dff73d51f',
    ),
    'composer/package-versions-deprecated' => 
    array (
      'pretty_version' => '1.11.99.1',
      'version' => '1.11.99.1',
      'aliases' => 
      array (
      ),
      'reference' => '7413f0b55a051e89485c5cb9f765fe24bb02a7b6',
    ),
    'concrete5/core' => 
    array (
      'pretty_version' => '1.0.0+no-version-set',
      'version' => '1.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'concrete5/doctrine-xml' => 
    array (
      'pretty_version' => '1.2.0',
      'version' => '1.2.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '03f1209d42477070a58d29e5af15688b96c66b56',
    ),
    'concrete5/oauth-user-data' => 
    array (
      'pretty_version' => 'v1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '416d5fd707e0b77b06a5be6d4eb9defa4aafb5a8',
    ),
    'concrete5/zend-queue' => 
    array (
      'pretty_version' => '0.9.0',
      'version' => '0.9.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c1993b008df062879d772570e28320acd60f7f95',
    ),
    'container-interop/container-interop' => 
    array (
      'pretty_version' => '1.2.0',
      'version' => '1.2.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '79cbf1341c22ec75643d841642dd5d6acd83bdb8',
    ),
    'container-interop/container-interop-implementation' => 
    array (
      'provided' => 
      array (
        0 => '^1.1',
      ),
    ),
    'dapphp/securimage' => 
    array (
      'pretty_version' => '3.6.8',
      'version' => '3.6.8.0',
      'aliases' => 
      array (
      ),
      'reference' => '5fc5953c4ffba1eb214cc83100672f238c184ca4',
    ),
    'defuse/php-encryption' => 
    array (
      'pretty_version' => 'v2.2.1',
      'version' => '2.2.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '0f407c43b953d571421e0020ba92082ed5fb7620',
    ),
    'doctrine/annotations' => 
    array (
      'pretty_version' => '1.11.1',
      'version' => '1.11.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'ce77a7ba1770462cd705a91a151b6c3746f9c6ad',
    ),
    'doctrine/cache' => 
    array (
      'pretty_version' => '1.10.2',
      'version' => '1.10.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '13e3381b25847283a91948d04640543941309727',
    ),
    'doctrine/collections' => 
    array (
      'pretty_version' => '1.6.7',
      'version' => '1.6.7.0',
      'aliases' => 
      array (
      ),
      'reference' => '55f8b799269a1a472457bd1a41b4f379d4cfba4a',
    ),
    'doctrine/common' => 
    array (
      'pretty_version' => '2.13.3',
      'version' => '2.13.3.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f3812c026e557892c34ef37f6ab808a6b567da7f',
    ),
    'doctrine/dbal' => 
    array (
      'pretty_version' => '2.12.1',
      'version' => '2.12.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'adce7a954a1c2f14f85e94aed90c8489af204086',
    ),
    'doctrine/event-manager' => 
    array (
      'pretty_version' => '1.1.1',
      'version' => '1.1.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '41370af6a30faa9dc0368c4a6814d596e81aba7f',
    ),
    'doctrine/inflector' => 
    array (
      'pretty_version' => '1.4.3',
      'version' => '1.4.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '4650c8b30c753a76bf44fb2ed00117d6f367490c',
    ),
    'doctrine/instantiator' => 
    array (
      'pretty_version' => '1.4.0',
      'version' => '1.4.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd56bf6102915de5702778fe20f2de3b2fe570b5b',
    ),
    'doctrine/lexer' => 
    array (
      'pretty_version' => '1.2.1',
      'version' => '1.2.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e864bbf5904cb8f5bb334f99209b48018522f042',
    ),
    'doctrine/migrations' => 
    array (
      'pretty_version' => 'v1.7.2',
      'version' => '1.7.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '3e7656c5adfd9b7d0d8757aa1e9e8daf43a6f200',
    ),
    'doctrine/orm' => 
    array (
      'pretty_version' => '2.7.5',
      'version' => '2.7.5.0',
      'aliases' => 
      array (
      ),
      'reference' => '01187c9260cd085529ddd1273665217cae659640',
    ),
    'doctrine/persistence' => 
    array (
      'pretty_version' => '1.3.8',
      'version' => '1.3.8.0',
      'aliases' => 
      array (
      ),
      'reference' => '7a6eac9fb6f61bba91328f15aa7547f4806ca288',
    ),
    'doctrine/reflection' => 
    array (
      'pretty_version' => '1.2.2',
      'version' => '1.2.2.0',
      'aliases' => 
      array (
      ),
      'reference' => 'fa587178be682efe90d005e3a322590d6ebb59a5',
    ),
    'egulias/email-validator' => 
    array (
      'pretty_version' => '1.2.17',
      'version' => '1.2.17.0',
      'aliases' => 
      array (
      ),
      'reference' => '19674b35a0a3456be1b96e137098d31ed386fb61',
    ),
    'filp/whoops' => 
    array (
      'pretty_version' => '2.9.2',
      'version' => '2.9.2.0',
      'aliases' => 
      array (
      ),
      'reference' => 'df7933820090489623ce0be5e85c7e693638e536',
    ),
    'gettext/gettext' => 
    array (
      'pretty_version' => 'v3.5.9',
      'version' => '3.5.9.0',
      'aliases' => 
      array (
      ),
      'reference' => '5b1d69f5889513f7ed65060ad2a662ec3b0875c7',
    ),
    'gettext/languages' => 
    array (
      'pretty_version' => '2.6.0',
      'version' => '2.6.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '38ea0482f649e0802e475f0ed19fa993bcb7a618',
    ),
    'guzzlehttp/guzzle' => 
    array (
      'pretty_version' => '6.5.5',
      'version' => '6.5.5.0',
      'aliases' => 
      array (
      ),
      'reference' => '9d4290de1cfd701f38099ef7e183b64b4b7b0c5e',
    ),
    'guzzlehttp/promises' => 
    array (
      'pretty_version' => '1.4.0',
      'version' => '1.4.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '60d379c243457e073cff02bc323a2a86cb355631',
    ),
    'guzzlehttp/psr7' => 
    array (
      'pretty_version' => '1.7.0',
      'version' => '1.7.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '53330f47520498c0ae1f61f7e2c90f55690c06a3',
    ),
    'hautelook/phpass' => 
    array (
      'pretty_version' => '0.3.5',
      'version' => '0.3.5.0',
      'aliases' => 
      array (
      ),
      'reference' => 'b4cbd9b67ed3ef5672ec79d8e0c46d24bd844abd',
    ),
    'htmlawed/htmlawed' => 
    array (
      'pretty_version' => '1.1.22',
      'version' => '1.1.22.0',
      'aliases' => 
      array (
      ),
      'reference' => 'b270453ba016ee4c6dae585f047d1e4f3cc456a1',
    ),
    'illuminate/config' => 
    array (
      'pretty_version' => 'v5.2.45',
      'version' => '5.2.45.0',
      'aliases' => 
      array (
      ),
      'reference' => '2db869c5b5a675cece410a0d0bc634dba0f69998',
    ),
    'illuminate/container' => 
    array (
      'pretty_version' => 'v5.2.45',
      'version' => '5.2.45.0',
      'aliases' => 
      array (
      ),
      'reference' => '5139cebc8293b6820b91aef6f4b4e18bde33c9b2',
    ),
    'illuminate/contracts' => 
    array (
      'pretty_version' => 'v5.2.45',
      'version' => '5.2.45.0',
      'aliases' => 
      array (
      ),
      'reference' => '22bde7b048a33c702d9737fc1446234fff9b1363',
    ),
    'illuminate/filesystem' => 
    array (
      'pretty_version' => 'v5.2.45',
      'version' => '5.2.45.0',
      'aliases' => 
      array (
      ),
      'reference' => '39668a50e0cf1d673e58e7dbb437475708cfb508',
    ),
    'illuminate/support' => 
    array (
      'pretty_version' => 'v5.2.21',
      'version' => '5.2.21.0',
      'aliases' => 
      array (
      ),
      'reference' => '6749fab3f3d38d8b15427536a8e7bbdc57497c9e',
    ),
    'imagine/imagine' => 
    array (
      'pretty_version' => '1.2.4',
      'version' => '1.2.4.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd2e18be6e930ca169e4f921ef73ebfc061bf55d8',
    ),
    'indigophp/hash-compat' => 
    array (
      'pretty_version' => 'v1.1.0',
      'version' => '1.1.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '43a19f42093a0cd2d11874dff9d891027fc42214',
    ),
    'kylekatarnls/update-helper' => 
    array (
      'pretty_version' => '1.2.1',
      'version' => '1.2.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '429be50660ed8a196e0798e5939760f168ec8ce9',
    ),
    'lcobucci/jwt' => 
    array (
      'pretty_version' => '3.4.3',
      'version' => '3.4.3.0',
      'aliases' => 
      array (
      ),
      'reference' => 'cd83b5421df3dbbbc66bd07a5b970c9efb6371b9',
    ),
    'league/csv' => 
    array (
      'pretty_version' => '8.2.3',
      'version' => '8.2.3.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd2aab1e7bde802582c3879acf03d92716577c76d',
    ),
    'league/event' => 
    array (
      'pretty_version' => '2.2.0',
      'version' => '2.2.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd2cc124cf9a3fab2bb4ff963307f60361ce4d119',
    ),
    'league/flysystem' => 
    array (
      'pretty_version' => '1.1.3',
      'version' => '1.1.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '9be3b16c877d477357c015cec057548cf9b2a14a',
    ),
    'league/flysystem-cached-adapter' => 
    array (
      'pretty_version' => '1.0.9',
      'version' => '1.0.9.0',
      'aliases' => 
      array (
      ),
      'reference' => '08ef74e9be88100807a3b92cc9048a312bf01d6f',
    ),
    'league/fractal' => 
    array (
      'pretty_version' => '0.17.0',
      'version' => '0.17.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a0b350824f22fc2fdde2500ce9d6851a3f275b0e',
    ),
    'league/mime-type-detection' => 
    array (
      'pretty_version' => '1.7.0',
      'version' => '1.7.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '3b9dff8aaf7323590c1d2e443db701eb1f9aa0d3',
    ),
    'league/oauth2-server' => 
    array (
      'pretty_version' => '5.1.6',
      'version' => '5.1.6.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a1a6cb7b4c7e61b5d2b40384c520b72f192d07c4',
    ),
    'league/oauth2server' => 
    array (
      'replaced' => 
      array (
        0 => '*',
      ),
    ),
    'league/openid-connect-claims' => 
    array (
      'pretty_version' => '1.1.0',
      'version' => '1.1.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '94c25fafdb36d1ee462dc1e74f95709943c1d363',
    ),
    'league/url' => 
    array (
      'pretty_version' => '3.3.5',
      'version' => '3.3.5.0',
      'aliases' => 
      array (
      ),
      'reference' => '1ae2c3ce29a7c5438339ff6388225844e6479da8',
    ),
    'lncd/oauth2' => 
    array (
      'replaced' => 
      array (
        0 => '*',
      ),
    ),
    'lusitanian/oauth' => 
    array (
      'pretty_version' => 'v0.8.11',
      'version' => '0.8.11.0',
      'aliases' => 
      array (
      ),
      'reference' => 'fc11a53db4b66da555a6a11fce294f574a8374f9',
    ),
    'michelf/php-markdown' => 
    array (
      'pretty_version' => '1.9.0',
      'version' => '1.9.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c83178d49e372ca967d1a8c77ae4e051b3a3c75c',
    ),
    'mlocati/concrete5-translation-library' => 
    array (
      'pretty_version' => '1.6.0',
      'version' => '1.6.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '4caccae6c1e28edbdccc619c527bd42dbedec331',
    ),
    'mlocati/ip-lib' => 
    array (
      'pretty_version' => '1.14.0',
      'version' => '1.14.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '882bc0e115970a536b13bcfa59f312783fce08c8',
    ),
    'mobiledetect/mobiledetectlib' => 
    array (
      'pretty_version' => '2.8.35',
      'version' => '2.8.35.0',
      'aliases' => 
      array (
      ),
      'reference' => '68a35170fdf36e7b35f9c125e5102338dbc3ff65',
    ),
    'monolog/monolog' => 
    array (
      'pretty_version' => '1.26.0',
      'version' => '1.26.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '2209ddd84e7ef1256b7af205d0717fb62cfc9c33',
    ),
    'natxet/cssmin' => 
    array (
      'pretty_version' => 'v3.0.6',
      'version' => '3.0.6.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd5d9f4c3e5cedb1ae96a95a21731f8790e38f1dd',
    ),
    'nesbot/carbon' => 
    array (
      'pretty_version' => '1.39.1',
      'version' => '1.39.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '4be0c005164249208ce1b5ca633cd57bdd42ff33',
    ),
    'ocramius/package-versions' => 
    array (
      'replaced' => 
      array (
        0 => '1.11.99',
      ),
    ),
    'ocramius/proxy-manager' => 
    array (
      'pretty_version' => '1.0.2',
      'version' => '1.0.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '57e9272ec0e8deccf09421596e0e2252df440e11',
    ),
    'pagerfanta/pagerfanta' => 
    array (
      'pretty_version' => 'v1.0.5',
      'version' => '1.0.5.0',
      'aliases' => 
      array (
      ),
      'reference' => '29aade64addfdfb12c05aabf160f09d1aea6b143',
    ),
    'paragonie/random_compat' => 
    array (
      'pretty_version' => 'v2.0.19',
      'version' => '2.0.19.0',
      'aliases' => 
      array (
      ),
      'reference' => '446fc9faa5c2a9ddf65eb7121c0af7e857295241',
    ),
    'patchwork/utf8' => 
    array (
      'pretty_version' => 'v1.3.3',
      'version' => '1.3.3.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e1fa4d4a57896d074c9a8d01742b688d5db4e9d5',
    ),
    'phpseclib/phpseclib' => 
    array (
      'pretty_version' => '2.0.30',
      'version' => '2.0.30.0',
      'aliases' => 
      array (
      ),
      'reference' => '136b9ca7eebef78be14abf90d65c5e57b6bc5d36',
    ),
    'primal/color' => 
    array (
      'pretty_version' => '1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'dca054972fd09cba33f51675878030305b85030a',
    ),
    'psr/cache' => 
    array (
      'pretty_version' => '1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd11b50ad223250cf17b86e38383413f5a6764bf8',
    ),
    'psr/cache-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0.0',
      ),
    ),
    'psr/container' => 
    array (
      'pretty_version' => '1.0.0',
      'version' => '1.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'b7ce3b176482dbbc1245ebf52b181af44c2cf55f',
    ),
    'psr/http-message' => 
    array (
      'pretty_version' => '1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f6561bf28d520154e4b0ec72be95418abe6d9363',
    ),
    'psr/http-message-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0',
      ),
    ),
    'psr/log' => 
    array (
      'pretty_version' => '1.1.3',
      'version' => '1.1.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '0f73288fd15629204f9d42b7055f72dacbe811fc',
    ),
    'psr/log-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0.0',
        1 => '1.0',
      ),
    ),
    'punic/calendar' => 
    array (
      'replaced' => 
      array (
        0 => '*',
      ),
    ),
    'punic/common' => 
    array (
      'replaced' => 
      array (
        0 => '*',
      ),
    ),
    'punic/punic' => 
    array (
      'pretty_version' => '3.5.1',
      'version' => '3.5.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '1b99a1f0d45e2a6109870fde7a3977d9da68940f',
    ),
    'ralouphie/getallheaders' => 
    array (
      'pretty_version' => '3.0.3',
      'version' => '3.0.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '120b605dfeb996808c31b6477290a714d356e822',
    ),
    'sunra/php-simple-html-dom-parser' => 
    array (
      'pretty_version' => 'v1.5.2',
      'version' => '1.5.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '75b9b1cb64502d8f8c04dc11b5906b969af247c6',
    ),
    'symfony/cache-contracts' => 
    array (
      'replaced' => 
      array (
        0 => 'v1.1.10',
      ),
    ),
    'symfony/class-loader' => 
    array (
      'pretty_version' => 'v3.4.47',
      'version' => '3.4.47.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a22265a9f3511c0212bf79f54910ca5a77c0e92c',
    ),
    'symfony/config' => 
    array (
      'pretty_version' => 'v3.3.2',
      'version' => '3.3.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '35716d4904e0506a7a5a9bcf23f854aeb5719bca',
    ),
    'symfony/console' => 
    array (
      'pretty_version' => 'v4.2.12',
      'version' => '4.2.12.0',
      'aliases' => 
      array (
      ),
      'reference' => 'fc2e274aade6567a750551942094b2145ade9b6c',
    ),
    'symfony/contracts' => 
    array (
      'pretty_version' => 'v1.1.10',
      'version' => '1.1.10.0',
      'aliases' => 
      array (
      ),
      'reference' => '011c20407c4b99d454f44021d023fb39ce23b73d',
    ),
    'symfony/debug' => 
    array (
      'pretty_version' => 'v4.4.19',
      'version' => '4.4.19.0',
      'aliases' => 
      array (
      ),
      'reference' => 'af4987aa4a5630e9615be9d9c3ed1b0f24ca449c',
    ),
    'symfony/deprecation-contracts' => 
    array (
      'pretty_version' => 'v2.2.0',
      'version' => '2.2.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '5fa56b4074d1ae755beb55617ddafe6f5d78f665',
    ),
    'symfony/event-dispatcher' => 
    array (
      'pretty_version' => 'v3.4.47',
      'version' => '3.4.47.0',
      'aliases' => 
      array (
      ),
      'reference' => '31fde73757b6bad247c54597beef974919ec6860',
    ),
    'symfony/event-dispatcher-contracts' => 
    array (
      'replaced' => 
      array (
        0 => 'v1.1.10',
      ),
    ),
    'symfony/filesystem' => 
    array (
      'pretty_version' => 'v3.4.47',
      'version' => '3.4.47.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e58d7841cddfed6e846829040dca2cca0ebbbbb3',
    ),
    'symfony/finder' => 
    array (
      'pretty_version' => 'v3.0.9',
      'version' => '3.0.9.0',
      'aliases' => 
      array (
      ),
      'reference' => '3eb4e64c6145ef8b92adefb618a74ebdde9e3fe9',
    ),
    'symfony/http-client-contracts' => 
    array (
      'replaced' => 
      array (
        0 => 'v1.1.10',
      ),
    ),
    'symfony/http-foundation' => 
    array (
      'pretty_version' => 'v3.4.47',
      'version' => '3.4.47.0',
      'aliases' => 
      array (
      ),
      'reference' => 'b9885fcce6fe494201da4f70a9309770e9d13dc8',
    ),
    'symfony/http-kernel' => 
    array (
      'pretty_version' => 'v3.4.47',
      'version' => '3.4.47.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a98a4c30089e6a2d52a9fa236f718159b539f6f5',
    ),
    'symfony/options-resolver' => 
    array (
      'pretty_version' => 'v5.2.3',
      'version' => '5.2.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '5d0f633f9bbfcf7ec642a2b5037268e61b0a62ce',
    ),
    'symfony/polyfill-ctype' => 
    array (
      'pretty_version' => 'v1.22.0',
      'version' => '1.22.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c6c942b1ac76c82448322025e084cadc56048b4e',
    ),
    'symfony/polyfill-intl-idn' => 
    array (
      'pretty_version' => 'v1.22.0',
      'version' => '1.22.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '0eb8293dbbcd6ef6bf81404c9ce7d95bcdf34f44',
    ),
    'symfony/polyfill-intl-normalizer' => 
    array (
      'pretty_version' => 'v1.22.0',
      'version' => '1.22.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '6e971c891537eb617a00bb07a43d182a6915faba',
    ),
    'symfony/polyfill-mbstring' => 
    array (
      'pretty_version' => 'v1.22.0',
      'version' => '1.22.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f377a3dd1fde44d37b9831d68dc8dea3ffd28e13',
    ),
    'symfony/polyfill-php56' => 
    array (
      'pretty_version' => 'v1.20.0',
      'version' => '1.20.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '54b8cd7e6c1643d78d011f3be89f3ef1f9f4c675',
    ),
    'symfony/polyfill-php70' => 
    array (
      'pretty_version' => 'v1.20.0',
      'version' => '1.20.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '5f03a781d984aae42cebd18e7912fa80f02ee644',
    ),
    'symfony/polyfill-php72' => 
    array (
      'pretty_version' => 'v1.22.0',
      'version' => '1.22.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'cc6e6f9b39fe8075b3dabfbaf5b5f645ae1340c9',
    ),
    'symfony/polyfill-php73' => 
    array (
      'pretty_version' => 'v1.22.0',
      'version' => '1.22.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a678b42e92f86eca04b7fa4c0f6f19d097fb69e2',
    ),
    'symfony/polyfill-php80' => 
    array (
      'pretty_version' => 'v1.22.0',
      'version' => '1.22.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'dc3063ba22c2a1fd2f45ed856374d79114998f91',
    ),
    'symfony/psr-http-message-bridge' => 
    array (
      'pretty_version' => 'v1.2.0',
      'version' => '1.2.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '9ab9d71f97d5c7d35a121a7fb69f74fee95cd0ad',
    ),
    'symfony/routing' => 
    array (
      'pretty_version' => 'v3.4.47',
      'version' => '3.4.47.0',
      'aliases' => 
      array (
      ),
      'reference' => '3e522ac69cadffd8131cc2b22157fa7662331a6c',
    ),
    'symfony/serializer' => 
    array (
      'pretty_version' => 'v3.4.47',
      'version' => '3.4.47.0',
      'aliases' => 
      array (
      ),
      'reference' => '6d69ccc1dcfb64c1e9c9444588643e98718d1849',
    ),
    'symfony/service-contracts' => 
    array (
      'replaced' => 
      array (
        0 => 'v1.1.10',
      ),
    ),
    'symfony/translation' => 
    array (
      'pretty_version' => 'v3.4.47',
      'version' => '3.4.47.0',
      'aliases' => 
      array (
      ),
      'reference' => 'be83ee6c065cb32becdb306ba61160d598b1ce88',
    ),
    'symfony/translation-contracts' => 
    array (
      'replaced' => 
      array (
        0 => 'v1.1.10',
      ),
    ),
    'symfony/yaml' => 
    array (
      'pretty_version' => 'v3.4.47',
      'version' => '3.4.47.0',
      'aliases' => 
      array (
      ),
      'reference' => '88289caa3c166321883f67fe5130188ebbb47094',
    ),
    'tedivm/jshrink' => 
    array (
      'pretty_version' => 'v1.4.0',
      'version' => '1.4.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '0513ba1407b1f235518a939455855e6952a48bbc',
    ),
    'tedivm/stash' => 
    array (
      'pretty_version' => 'v0.14.2',
      'version' => '0.14.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '7ea9749784152dcd2dab72c4bbf2bef18c326e41',
    ),
    'theorchard/monolog-cascade' => 
    array (
      'pretty_version' => '0.5.3',
      'version' => '0.5.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '034f018e9814c7a057ad2f64242f25e07aaad174',
    ),
    'true/punycode' => 
    array (
      'pretty_version' => 'v2.1.1',
      'version' => '2.1.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a4d0c11a36dd7f4e7cd7096076cab6d3378a071e',
    ),
    'voku/portable-utf8' => 
    array (
      'pretty_version' => '1.6.18',
      'version' => '1.6.18.0',
      'aliases' => 
      array (
      ),
      'reference' => 'b49c259b0f5c563422d466d10802c5db35328000',
    ),
    'voku/urlify' => 
    array (
      'pretty_version' => '1.0.10-stable',
      'version' => '1.0.10.0',
      'aliases' => 
      array (
      ),
      'reference' => 'ff7ba3bd53e4612fd34e9003928d804c4b212f6a',
    ),
    'wikimedia/less.php' => 
    array (
      'pretty_version' => '1.8.2',
      'version' => '1.8.2.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e238ad228d74b6ffd38209c799b34e9826909266',
    ),
    'zendframework/zend-cache' => 
    array (
      'pretty_version' => '2.7.2',
      'version' => '2.7.2.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c98331b96d3b9d9b24cf32d02660602edb34d039',
    ),
    'zendframework/zend-code' => 
    array (
      'pretty_version' => '2.6.3',
      'version' => '2.6.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '95033f061b083e16cdee60530ec260d7d628b887',
    ),
    'zendframework/zend-escaper' => 
    array (
      'pretty_version' => '2.6.1',
      'version' => '2.6.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '3801caa21b0ca6aca57fa1c42b08d35c395ebd5f',
    ),
    'zendframework/zend-eventmanager' => 
    array (
      'pretty_version' => '3.2.1',
      'version' => '3.2.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a5e2583a211f73604691586b8406ff7296a946dd',
    ),
    'zendframework/zend-feed' => 
    array (
      'pretty_version' => '2.7.0',
      'version' => '2.7.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '12b328d382aa5200f1de53d4147033b885776b67',
    ),
    'zendframework/zend-http' => 
    array (
      'pretty_version' => '2.6.0',
      'version' => '2.6.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '09f4d279f46d86be63171ff62ee0f79eca878678',
    ),
    'zendframework/zend-hydrator' => 
    array (
      'pretty_version' => '1.1.0',
      'version' => '1.1.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '22652e1661a5a10b3f564cf7824a2206cf5a4a65',
    ),
    'zendframework/zend-i18n' => 
    array (
      'pretty_version' => '2.7.4',
      'version' => '2.7.4.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd3431e29cc00c2a1c6704e601d4371dbf24f6a31',
    ),
    'zendframework/zend-loader' => 
    array (
      'pretty_version' => '2.6.1',
      'version' => '2.6.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '91da574d29b58547385b2298c020b257310898c6',
    ),
    'zendframework/zend-mail' => 
    array (
      'pretty_version' => '2.7.3',
      'version' => '2.7.3.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e92b4bc1cf6fe0fdad571bd7b4af2762414d58ff',
    ),
    'zendframework/zend-mime' => 
    array (
      'pretty_version' => '2.7.2',
      'version' => '2.7.2.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c91e0350be53cc9d29be15563445eec3b269d7c1',
    ),
    'zendframework/zend-servicemanager' => 
    array (
      'pretty_version' => '3.1.2',
      'version' => '3.1.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '8a6078959a2e8c3717ee4945d4a2d9f3ddf81d38',
    ),
    'zendframework/zend-stdlib' => 
    array (
      'pretty_version' => '2.7.7',
      'version' => '2.7.7.0',
      'aliases' => 
      array (
      ),
      'reference' => '0e44eb46788f65e09e077eb7f44d2659143bcc1f',
    ),
    'zendframework/zend-uri' => 
    array (
      'pretty_version' => '2.7.1',
      'version' => '2.7.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'bfc4a5b9a309711e968d7c72afae4ac50c650083',
    ),
    'zendframework/zend-validator' => 
    array (
      'pretty_version' => '2.11.1',
      'version' => '2.11.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '3c28dfe4e5951ba38059cea895244d9d206190b3',
    ),
  ),
);
