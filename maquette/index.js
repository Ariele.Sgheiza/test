const express = require('express') 
var cors = require('cors');
var app = express();
app.use(cors());


app.get('/', function (req, res) {
    res.send('Funziona!')
})
app.listen(2555, function () {
console.log('Example app listening on port 2555! ')
})

const mysql = require('mysql')

  let con = mysql.createConnection({
    host: "129.194.10.121",
    user: "root",
    password: "password",
    database: "portfolio"
  });
  
  con.connect(function (err) {
    if (err) {
      console.log("mysql Erreur, démarrer la base de donnée")
      throw err;
    }
    console.log('Connected to DataBase!');
  });

  //projet
  app.get('/getProjets',function(req,res){
    con.query('SELECT * FROM Projet',(err,results)=>{
      if(err) throw err;
      res.send(JSON.stringify(results));
    });
  });

  //Etudiant
  app.get('/getEtudiants',function(req,res){
    con.query('SELECT * FROM Etudiant',(err,results)=>{
      if(err) throw err;
      res.send(JSON.stringify(results));
    });
  });

  app.get('/getCours',function(req,res){
    con.query('SELECT Cours.id_cours, Cours.nom_cours FROM Cours',(err,results)=>{
      if(err) throw err;
      res.send(JSON.stringify(results));
    });
  });

  app.get('/getInfosCours',function(req,res){
    con.query("SELECT Cours.nom_cours,Cours.faculte,Cours.prof_cours FROM Cours WHERE id_cours='"+req.query.idCours+"'",(err,results)=>{
      if(err) throw err;
      res.send(JSON.stringify(results));
    });
  });

  app.get('/getCategories',function(req,res){
    con.query('SELECT Categorie.id_cat, Categorie.nom_cat FROM Categorie',(err,results)=>{
      if(err) throw err;
      res.send(JSON.stringify(results));
    });
  });

  app.get('/getCategoriesProjet',function(req,res){
    con.query("SELECT Categorie.nom_cat FROM ProjetCategorie,Categorie WHERE ProjetCategorie.id_projet='"+req.query.idProjet+"' AND ProjetCategorie.id_cat=Categorie.id_cat",(err,results)=>{
      if(err) throw err;
      res.send(JSON.stringify(results));
    });
  });

app.get('/recherche', function(req,res){
  console.log(req.query.motClef);
  //il faut faire une recherche par nom etudiant, nom prof, nom cours, titre projet
  con.query(
    'SELECT Projet.titre_projet, Projet.id_projet FROM Projet, Cours, Etudiant, EtudiantProjet WHERE Projet.titre_projet LIKE "%' + req.query.motClef + '%"'
    +' OR Cours.nom_cours LIKE "%' + req.query.motClef + '%" AND Cours.id_cours = Projet.id_cours'
    +' OR Cours.prof_cours LIKE "%' + req.query.motClef + '%" AND Cours.id_cours = Projet.id_cours'
    +' OR Etudiant.nom_etu LIKE "%' + req.query.motClef + '%" AND Etudiant.id_etu = EtudiantProjet.id_etu AND EtudiantProjet.id_projet = Projet.id_projet'
    +' GROUP BY Projet.titre_projet,Projet.id_projet,Projet.vues_projet ORDER BY Projet.vues_projet ASC '
    ,(err,results)=>{
      if(err) throw err;
      //console.log(JSON.stringify(results));
      res.send(JSON.stringify(results));
    }
  );
});

app.get('/getProjetInfos',function(req,res){
  con.query('SELECT Projet.*, Etudiant.nom_etu, Etudiant.prenom_etu, Etudiant.mail_etu, Etudiant.id_etu FROM Projet, Etudiant, EtudiantProjet WHERE Projet.id_projet=' + req.query.idProjet
    +' AND Projet.id_projet = EtudiantProjet.id_projet AND EtudiantProjet.id_etu = Etudiant.id_etu'
    //+' AND Projet.id_projet = ProjetCategorie.id_projet AND ProjetCategorie.id_cat = Categorie.id_cat' //Categories pas encore saisies
    ,(err,results)=>{
      if(err) throw err;
      console.log(results);
      res.send(JSON.stringify(results));
    }
  );
});


app.get('/getProfilInfos',function(req,res){
  con.query('SELECT Etudiant.*, Projet.titre_projet, Projet.id_projet FROM Etudiant, EtudiantProjet, Projet WHERE Etudiant.id_etu=' + req.query.idEtudiant
  + " AND Etudiant.id_etu = EtudiantProjet.id_etu AND EtudiantProjet.id_projet = Projet.id_projet"
    ,(err,results)=>{
      if(err) throw err;
      console.log(results);
      res.send(JSON.stringify(results));
    }
  );
});

app.get('/testConnexion',function(req,res){
  console.log(req);
  con.query('SELECT Etudiant.id_etu FROM Etudiant WHERE Etudiant.prenom_etu = "' + req.query.prenom
  + '" AND Etudiant.nom_etu = "' + req.query.nom + '";'
    ,(err,results)=>{
      if(err) throw err;
      console.log(results);
      res.send(JSON.stringify(results));
    }
  );
});

app.post('/addProjet', function (req, res) {
  console.log(req);
  /*
  con.query('INSERT INTO LivresRendus SET pseudo = "'+req.body.pseudo
              +'", idLivre ="'+req.body.idLivre
              +'";'
  , function (error, results, fields) {
      if (error) 
          throw error;
      res.send(results);
  });*/
});