/*
function test3(){
    axios.get('http://127.0.0.1:2555/test/', { params: { id : 1 } })
        .then(response => {
            console.log(response.data[0].titre_projet); //les données se trouve dans response.data et data est un array de données
            document.getElementById("test").innerHTML=response.data[0].titre_projet;
        })
        .catch(error => {
            console.log(error)
        })
    
}
*/

function chargerPageProfil(idEtu){
    if(typeof(Storage)!=="undefined")
        localStorage.id_etudiant=idEtu;
    else
        console.log("Sorry, your browser does not support web storage...");
    window.location='profil.html';
}

function chargerPageProjet(idProjet){
    if(typeof(Storage)!=="undefined")
        localStorage.id_projet=idProjet;
    else
        console.log("Sorry, your browser does not support web storage...");
    window.location='projet.html';
}

function chargerPageCanvas(){
    window.location='canvas.html';
}

function addRechercheResultat(nomProjet, idProjet) {
    
    document.getElementById('projets').innerHTML +="<div class='recherche' onclick='chargerPageProjet("+idProjet+")'>"+" <img src='images/noresult.png' alt='noresult' class='noresult'>"+"<p class='nomProjet'>"+nomProjet+"</p>"+"</div>" ;
    //changer adElement() par la méthode qui permettra de charger la page d'un projet
    
}

function lancerRecherche(){ 
    if(typeof(Storage)!=="undefined")
        localStorage.recherche= document.getElementById("search").value;
    else
        console.log("Sorry, your browser does not support web storage...");
    window.location='recherche.html';
}

function rechercher(motRecherche){
    axios.get('http://127.0.0.1:2555/recherche/', { params: { motClef : motRecherche } })
        .then(response => {
            if(response.data.length == 0){
                document.getElementById('projets').innerHTML += "Aucun projet ne correspond à votre recherche."
            }else{
                var listeTitresProjets = "";
                for(var i = 0; i < response.data.length; i++){
                    listeTitresProjets = listeTitresProjets + response.data[i].titre_projet;
                    if (i < response.data.length-1)
                        listeTitresProjets = listeTitresProjets + ", ";
                    addRechercheResultat(response.data[i].titre_projet, response.data[i].id_projet)
                }
            }
            //document.getElementById("test").innerHTML= listeTitresProjets;
        })
        .catch(error => {
            console.log(error)
        })
}

//afficher les infos d'un projet
function getProjetInfos(id_projet){
    axios.get('http://127.0.0.1:2555/getProjetInfos/', { params: { idProjet : id_projet } })
        .then(response => {
            console.log(response.data);
            document.getElementById('hautPage').innerHTML+="<p id='titreProjetHaut'>"+response.data[0].titre_projet+"</p>";
            document.getElementById('infosProjetPage').innerHTML+="<p id='descriptionContent'>"+response.data[0].desc_projet+"</p>"+"<h1 class='titrePageProjet'>Documents</h1>";
            
            axios.get('http://127.0.0.1:2555/getInfosCours/', { params: { idCours : response.data[0].id_cours}})
            .then(response => {

                console.log(response.data);
                document.getElementById('infoCoursProjet').innerHTML+="<p class='descAsideProjet' id='coursProjet'>"+response.data[0].nom_cours+"</p>";
                document.getElementById('infoCoursProjet').innerHTML+="<p class='descAsideProjet' id='faculteProjet'>"+response.data[0].faculte+"</p>";
                document.getElementById('infoCoursProjet').innerHTML+="<p class='descAsideProjet' id='profProjet'>"+response.data[0].prof_cours+"</p>";
            })
            .catch(error => {
            console.log(error)
             })
            
             axios.get('http://127.0.0.1:2555/getCategoriesProjet/', { params: { idProjet : response.data[0].id_projet}})
             .then(response => {
 
                 console.log(response.data);
                 document.getElementById('sideProjet').innerHTML+="<p class='descAsideProjet' id='catProjet'>"+response.data[0].nom_cat+"</p>";
             })
             .catch(error => {
             console.log(error)
              })

            for(var i = 0; i < response.data.length; i++){
                document.getElementById('profilContainer').innerHTML+="<img src='images/profil.png' alt='profil' class='BoutonsVersProfil' onclick='chargerPageProfil("+response.data[i].id_etu+")'>";
               
            }
            document.getElementById("infosProjet").innerHTML = texte;
        })
        .catch(error => {
            console.log(error)
        })
}

//afficher les infos d'un profil
function getProfilInfos(idEtu){
    axios.get('http://127.0.0.1:2555/getProfilInfos/', { params: { idEtudiant : idEtu } })
        .then(response => {
            console.log(response.data);
            document.getElementById('hautPage').innerHTML+="<p id='nomEtudiant'>"+response.data[0].prenom_etu+" "+response.data[0].nom_etu+"</p>";
            document.getElementById('infoEtudiant').innerHTML+="<p id='descEtudiant'>"+response.data[0].desc_etu+"</p>";


            //var texte = "";
            //texte += "Nom etudiant : " + response.data[0].nom_etu + "<br>";
            //texte += "Prenom etudiant : " + response.data[0].prenom_etu + "<br>";
            //texte += "Mail etudiant : " + response.data[0].mail_etu + "<br><br><br>";
            //document.getElementById("infosEtudiant").innerHTML = texte;

            if(localStorage.id_etudiant_connectée == localStorage.id_etudiant){
                document.getElementById('projets').innerHTML += "<div class='recherche' onclick='chargerPageCanvas()'>"+" <img src='images/noresult.png' alt='noresult' class='noresult'>"+"<p class='nomProjet'>"+ "Créer un projet" +"</p>"+"</div>" ;
            }
            for(var i = 0; i < response.data.length; i++)
                document.getElementById('projets').innerHTML += "<div class='recherche' onclick='chargerPageProjet("+response.data[i].id_projet+")'>"+" <img src='images/noresult.png' alt='noresult' class='noresult'>"+"<p class='nomProjet'>"+response.data[i].titre_projet+"</p>"+"</div>" ;
                document.getElementById('infoEtudiant').innerHTML+="<p id=titreNbProjetProfil>Nombre de projet:</p> <p id='nbProjets'>"+i+"</p>";

        })
        .catch(error => {
            console.log(error)
        })
}

function connexion(saisiNom, saisiPrenom){
    //requete pour vérifier si saisie est bonne
    axios.get('http://127.0.0.1:2555/testConnexion/', { params: { nom : saisiNom, prenom : saisiPrenom } })
        .then(response => {
            console.log(response.data);

            //stocker info que personne est connectée
            if(typeof(Storage)!=="undefined")
                localStorage.id_etudiant_connectée=response.data[0].id_etu;
            else
                console.log("Sorry, your browser does not support web storage...");  
            
            //charger page
            chargerPageProfil(response.data[0].id_etu);
        })
        .catch(error => {
            console.log(error)
        })
    
    
}

function chargerCategorieEtCours(){
    axios.get('http://127.0.0.1:2555/getCours/', {  })
        .then(response => {
            console.log(response.data);
            
            for(var i = 0; i < response.data.length; i++)
                document.getElementById('selectCours').innerHTML += "<option value='" + response.data[i].id_cours + "'>" + response.data[i].nom_cours + "</option>";
            
        })
        .catch(error => {
            console.log(error)
        })

    axios.get('http://127.0.0.1:2555/getCategories/', {  })
        .then(response => {
            console.log(response.data);
            
            for(var i = 0; i < response.data.length; i++)
                document.getElementById('selectCategorie').innerHTML += "<option value='" + response.data[i].id_cat + "'>" + response.data[i].nom_cat + "</option>";
            
        })
        .catch(error => {
            console.log(error)
        })
}

function creerProjet(){
    var cours = document.getElementById('selectCours').value;
    var categorie = document.getElementById('selectCategorie').value;

    var titreProjet = document.getElementById('titreProjet').value;
    var descriptionProjet = document.getElementById('descriptionProjet').value;
    var telechargeable = document.getElementById('telechargeable').value;
    console.log(
        {   titre : titreProjet, 
            idEtu : localStorage.id_etudiant_connectée, 
            descProjet : descriptionProjet,
            idCours : cours,
            idCat : categorie,
            telecharger : telechargeable});
    axios.post('http://127.0.0.1:2555/addProjet/', {titre : titreProjet}
    
    ).then(response => {
            console.log(response.data);
        })
        .catch(error => {
            console.log(error)
        })
}

